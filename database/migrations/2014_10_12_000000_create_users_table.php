<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('role', ['admin', 'management', 'employee'])->default('employee');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->text('about')->nullable();
            $table->string('phone')->nullable();
            $table->string('image')->nullable();
            $table->string('password');
            $table->enum('status', ['active', 'deactivate'])->default('deactivate');
            $table->integer('sort')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('project_name')->nullable();
            $table->longText('description')->nullable();
            $table->date('start_date');
            $table->date('end_date');
            $table->tinyInteger('ratio')->nullable();
            $table->enum('status', ['active', 'deactivate'])->default('deactivate');
            $table->integer('sort')->default(0);
            $table->timestamps();
        });

        Schema::create('tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('super_vision')->nullable();
            $table->integer('project_id')->nullable();
            $table->string('name');
            $table->longText('task_details');
            $table->date('start_date');
            $table->date('end_date');
            $table->tinyInteger('ratio')->nullable();
            $table->text('note')->nullable();
            $table->enum('status', ['active', 'deactivate'])->default('deactivate');
            $table->integer('sort')->default(0);
            $table->timestamps();
        });

        Schema::create('task_lists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('task_id');
            $table->string('list_name');
            $table->timestamps();
        });

        Schema::create('task_employee_relations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('employee_id');
            $table->bigInteger('task_id');
            $table->timestamps();
        });

        Schema::create('works', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('task_id');
            $table->integer('employee_id');
            $table->longText('done_task_details');
            $table->text('note');
            $table->enum('status', ['pending', 'working', 'complete', 'rejected'])->default('pending');
            $table->integer('sort')->default(0);
            $table->timestamps();
        });

        Schema::create('images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('task_id')->nullable();
            $table->integer('work_id')->nullable();
            $table->string('image');
            $table->enum('status', ['active', 'deactivate'])->default('active');
            $table->integer('sort')->default(0);
            $table->timestamps();
        });

        Schema::create('reviews', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('employee_id')->nullable();
            $table->integer('task_id')->nullable();
            $table->integer('work_id')->nullable();
            $table->enum('rating', ['1', '2', '3', '4', '5'])->nullable();
            $table->string('comments')->nullable();
            $table->date('date')->nullable();
            $table->integer('sort')->default(0);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('projects');
        Schema::dropIfExists('tasks');
        Schema::dropIfExists('task_lists');
        Schema::dropIfExists('task_employee_relations');
        Schema::dropIfExists('works');
        Schema::dropIfExists('reviews');
    }
}
