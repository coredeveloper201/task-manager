<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html dir="ltr">


<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>MediusWare TaskMan</title>
</head>

<body style="margin:0px; background: #f8f8f8; ">
<div width="100%" style="background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;">
    <div style="max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
            <tbody>
            <tr>
                <td style="vertical-align: top; padding-bottom:30px;" align="center">
                    <a href="https://mediusware.com/" target="_blank">
                        <img src="{!! asset('logo.png') !!}" alt="MediusWare" style="border:none; width: 140px">
                    </a>
                </td>
            </tr>
            </tbody>
        </table>
        @yield('mail-content')
        <div style="text-align: center; font-size: 12px; color: #b2b2b5; margin-top: 20px">
            <p> Powered by Mediusware.com
                <br>
                <a href="https://mediusware.com/" target="_blank" style="color: #b2b2b5; text-decoration: underline;">Subscribe Now</a> </p>
        </div>
    </div>
</div>
</body>

</html>
