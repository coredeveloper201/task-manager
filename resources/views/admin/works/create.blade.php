@extends('admin.master')

@section('title')
    Mediusware | Work Create
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Work Create</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/home') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('work.index') !!}">Works</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Create</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- End Bread crumb and right sidebar toggle -->
    
    
    <!-- Container fluid  -->

    <div class="page-content container-fluid">

        @include('admin.include.alert')

        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10 col-sm-8">
                                <h4 class="card-title">Work Create</h4>
                            </div>
                            <div class="col-md-2 col-sm-4 text-right">
                                <a href="{!! route('work.index') !!}" class="btn btn-success"><i class="fa fa-arrow-left"></i> Back</a>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>

                        <form class="" method="post" action="{!! route('work.store') !!}" novalidate enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-7 col-md-7 col-xs-12">
{{--                                    <div class="form-group">--}}
{{--                                        <h5>Select Task<span class="text-danger">*</span></h5>--}}
{{--                                        <div class="controls">--}}
{{--                                            <select class="form-control{{ $errors->has('employee') ? ' is-invalid' : '' }}" name="task_id" required data-validation-required-message="This field is required">--}}
{{--                                                @if(!empty($tasks))--}}
{{--                                                    <option>-- Select Task --</option>--}}
{{--                                                    @foreach($tasks as $task)--}}
{{--                                                    <option value="{!! $task->task_id !!}">{!! $task->task_name !!}</option>--}}
{{--                                                    @endforeach--}}
{{--                                                @else--}}
{{--                                                    <option>-- Task not available --</option>--}}
{{--                                                @endif--}}
{{--                                            </select>--}}
{{--                                            @if ($errors->has('employee'))--}}
{{--                                                <span class="invalid-feedback" role="alert">--}}
{{--                                                    <strong>{{ $errors->first('employee') }}</strong>--}}
{{--                                                </span>--}}
{{--                                            @endif--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

                                    <input type="hidden" name="task_id" value="{!! $taskId !!}">
                                    <div class="form-group">
                                        <h5>Work Details<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <textarea  name="work_details"  class="form-control{{ $errors->has('work_details') ? ' is-invalid' : '' }} summernote" required data-validation-required-message="This field is required">{!! old('work_details') !!}</textarea>
                                            @if ($errors->has('work_details'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('work_details') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <h5>Work Note</h5>
                                        <div class="controls">
                                            <textarea  name="note"  class="form-control{{ $errors->has('note') ? ' is-invalid' : '' }}" >{!! old('note') !!}</textarea>
                                            @if ($errors->has('note'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('note') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group validate">
                                        <h5>Status <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <fieldset>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" name="status" checked value="pending" required="" id="pending" class="custom-control-input" aria-invalid="false">
                                                    <label class="custom-control-label" for="pending">Pending</label>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio"  name="status" value="working" id="working" class="custom-control-input" aria-invalid="false">
                                                    <label class="custom-control-label" for="working">Working</label>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio"  name="status" value="complete" id="complete" class="custom-control-input" aria-invalid="false">
                                                    <label class="custom-control-label" for="complete">Complete</label>
                                                </div>
                                            </fieldset>

                                            @if ($errors->has('status'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('status') }}</strong>
                                                </span>
                                            @endif
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <hr>

                                </div>
                                <div class="col-lg-5 col-md-5 col-xs-12">
                                    <div class="form-group">
                                        <h5>Work Image<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="file" name="images[]" multiple>
                                            @if ($errors->has('images'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('images') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            </div>

                            <div class="text-xs-right">
                                <button type="submit" class="btn btn-info">Submit</button>
                                <button type="reset" class="btn btn-inverse">Reset</button>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
        
        <!-- First Cards Row  -->
    </div>


@endsection

@section('page_js')
    <script>
        $(document).ready(function() {
            $('input[name="images[]"]').fileuploader({
                addMore: true,
                files: null,
            });
        });

    </script>
@endsection
