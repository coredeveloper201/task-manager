@extends('admin.master')

@section('title')
    Mediusware | Late Report
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Late Report</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/home') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Late Report</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    
    <!-- End Bread crumb and right sidebar toggle -->
    
    <!-- Container fluid  -->

    <div class="page-content container-fluid">

    @include('admin.include.alert')
        <!-- First Cards Row  -->
        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10">
                                <h4 class="card-title">All Users</h4>
                            </div>
                            <div class="col-md-2 text-right">
                                <a href="{!! route('late_report.list') !!}" class="btn btn-success"><i class="fa fa-list"></i> Late Report List</a>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <form action="{{ route('late-report.store') }}" method="post">
                                @csrf
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <h5>Date<span class="text-danger">*</span></h5>
                                        <div class="input-group">
                                            <input type="text" class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }} datepicker-autoclose" value="{!! old('date', date('Y-m-d')) !!}" required data-validation-required-message="This field is required" name="date"  placeholder="Date">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="icon-calender"></i></span>
                                            </div>
                                            @if ($errors->has('date'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('date') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <table class="table table-striped table-hover table-bordered display" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th width="8%">Avatar</th>
                                        <th>Name</th>
                                        <th>10 to 11</th>
                                        <th>11 to 12</th>
                                        <th>After 12</th>
                                        <th>Remarks</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tablecontents">
                                        @foreach($users as $key => $user)
                                            <input type="hidden" name="user_id[]" value="{!! $user->id !!}">
                                        <tr class="row1" data-id="{{ $user->id }}">
                                            <td class="text-center">
                                                @if($user->image)
                                                    <img src="{!! asset('media/user/'.$user->image) !!}" width="50">
                                                @else
                                                    <img src="{!! asset('avatar.png') !!}" width="50">
                                                @endif
                                            </td>
                                            <td>{!! $user->name !!}</td>

                                            <td>
                                                <div class="controls">
                                                    <fieldset>
                                                        <div class="custom-control custom-radio">
                                                            <input type="radio" name="in_time[{{ $user->id }}]" value="10_11"  id="status1-{{ $user->id }}" class="custom-control-input" aria-invalid="false">
                                                            <label class="custom-control-label" for="status1-{{ $user->id }}">Active</label>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="controls">
                                                    <fieldset>
                                                        <div class="custom-control custom-radio">
                                                            <input type="radio" name="in_time[{{ $user->id }}]" value="11_12"  id="status2-{{ $user->id }}" class="custom-control-input" aria-invalid="false">
                                                            <label class="custom-control-label" for="status2-{{ $user->id }}">Active</label>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </td>

                                            <td>
                                                <div class="controls">
                                                    <fieldset>
                                                        <div class="custom-control custom-radio">
                                                            <input type="radio" name="in_time[{{ $user->id }}]" value="12" id="status3-{{ $user->id }}" class="custom-control-input" aria-invalid="false">
                                                            <label class="custom-control-label" for="status3-{{ $user->id }}">Active</label>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </td>

                                            <td>
                                                <div class="controls">
                                                    <textarea  name="remarks[{{ $user->id }}]"  class="form-control{{ $errors->has('remarks') ? ' is-invalid' : '' }}" >{!! old('remarks') !!}</textarea>
                                                    @if ($errors->has('remarks'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('remarks') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <button type="submit" class="btn btn-info">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>




@endsection

@section('page_js')

@endsection
