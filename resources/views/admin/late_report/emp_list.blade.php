@extends('admin.master')

@section('title')
    Mediusware | Late Report
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Late Report</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/home') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Late Report</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    
    <!-- End Bread crumb and right sidebar toggle -->
    
    <!-- Container fluid  -->

    <div class="page-content container-fluid">

    @include('admin.include.alert')
        <!-- First Cards Row  -->
        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10">
                                <h4 class="card-title">All employees late report</h4>
                            </div>
                            <div class="col-md-2 text-right">
                                <a href="{!! route('emp-late-report.monthly') !!}" class="btn btn-success"><i class="fa fa-lira-sign"></i> Monthly Report</a>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>
                        <form action="{!! url('taskman/late-report/filter') !!}" method="get">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <h5>Start Date</h5>
                                        <div class="input-group">
                                            <input type="text" class="form-control{{ $errors->has('start_date') ? ' is-invalid' : '' }} datepicker-autoclose" value="{!! !empty($data) ? $data['start_date']:date('Y-m-d') !!}" required data-validation-required-message="This field is required" name="start_date"  placeholder="Date">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="icon-calender"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <h5>End Date</h5>
                                        <div class="input-group">
                                            <input type="text" class="form-control{{ $errors->has('end_date') ? ' is-invalid' : '' }} datepicker-autoclose" value="{!! !empty($data) ? $data['end_date']:date('Y-m-d') !!}" required data-validation-required-message="This field is required" name="end_date"  placeholder="Date">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="icon-calender"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <br>
                                        <div class="controls">
                                            <button type="submit" class="btn btn-info">Go</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="table-responsive">
                            <table id="row_create_call" class="table table-striped table-hover table-bordered display" style="width:100%">
                                <thead>
                                <tr>
                                    <th width="8%">Avatar</th>
                                    <th>Name</th>
                                    <th>In Time</th>
                                    <th>Date</th>
                                    <th>Remarks</th>
                                </tr>
                                </thead>
                                <tbody id="tablecontents">
                                @foreach($lateReports as $lateReport)
                                    <tr>
                                        <td class="text-center">
                                            @if($lateReport->user->image)
                                                <img src="{!! asset('media/user/'.$lateReport->user->image) !!}" width="50">
                                            @else
                                                <img src="{!! asset('avatar.png') !!}" width="50">
                                            @endif
                                        </td>
                                        <td>{!! $lateReport->user->name !!}</td>
                                        <td>
                                            @if($lateReport->in_time == "10_11")
                                                <span class="text-warning">10 to 11</span>
                                            @elseif($lateReport->in_time == "11_12")
                                                <span class="text-warning">11 to 12</span>
                                            @elseif($lateReport->in_time == "12")
                                                <span class="text-danger">After 12 pm</span>
{{--                                            @else--}}
{{--                                                <span class="text-success">Late Allowed</span>--}}
                                            @endif
                                        </td>
                                        <td>{!! date('F d, Y', strtotime($lateReport->date)) !!}</td>
                                        <td>{!! $lateReport->remarks !!}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>




@endsection

@section('page_js')

@endsection
