<div class="col-lg-7 col-md-7 col-xs-12">
    <div class="form-group">
        <h5>Select Users<span class="text-danger">*</span></h5>
        <div class="controls">
            <select class="form-control{{ $errors->has('user_id') ? ' is-invalid' : '' }}" name="user_id">
                <option>-- Select Users --</option>
                @if(!empty($users))
                    @foreach($users as $user)
                        <option value="{{ $user->id }}" {{ isset($data) && $data->user_id == $user->id ? 'selected':'' }}>{{ $user->name }}</option>
                    @endforeach
                @endif
            </select>
            @if ($errors->has('user_id'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('user_id') }}</strong>
                </span>
            @endif
        </div>
    </div>
</div>

<div class="col-lg-7 col-md-7 col-xs-12">
    <div class="form-group">
        <h5>Date<span class="text-danger">*</span></h5>
        <div class="controls">
            <input type="text" name="date" value="{!! old('date', isset($data) ? $data->date:date('Y-m-d')) !!}" class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }} datepicker-autoclose" required data-validation-required-message="This field is required">
            @if ($errors->has('date'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('date') }}</strong>
                </span>
            @endif
        </div>
    </div>
</div>

<div class="col-lg-7 col-md-7 col-xs-12">
    <div class="form-group">
        <h5>Comment<span class="text-danger">*</span></h5>
        <div class="controls">
            <textarea name="comment" class="form-control{{ $errors->has('comment') ? ' is-invalid' : '' }} summernote" required data-validation-required-message="This field is required">{!! old('comment', isset($data) ? $data->comment:'') !!}</textarea>
            @if ($errors->has('comment'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('comment') }}</strong>
                </span>
            @endif
        </div>
    </div>
</div>
