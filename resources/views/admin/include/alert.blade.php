@if (session('success'))
    <div class="alert alert-success"> <i class="fa fa-check-circle"></i>
        <b> {{ session('success') }}</b>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
    </div>
@elseif(session('error'))
    <div class="alert alert-danger"> <i class="fa fa-exclamation-triangle"></i>
        <b> {{ session('error') }}</b>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
    </div>
@endif
