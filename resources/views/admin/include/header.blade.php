<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-dark">
        <div class="navbar-header border-right">
            <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
            <a class="navbar-brand" href="{{ url('taskman/dashboard') }}">
                <b class="logo-icon">
                    <img src="{!! asset('admin') !!}/images/logos/logo-icon.png" alt="homepage" class="dark-logo" style="width: 100%" />
                    <!-- Light Logo icon -->
                    <img src="{!! asset('admin') !!}/images/logos/logo-light-icon.png" alt="homepage" class="light-logo" style="width: 100%" />
                </b>
                <span class="logo-text">
                    <img src="{!! asset('admin') !!}/images/logos/logo-text.png" alt="homepage" class="dark-logo" />
                    <img src="{!! asset('admin') !!}/images/logos/logo-light-text.png" class="light-logo" alt="homepage" />
                </span>
            </a>
            <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="ti-more"></i></a>
        </div>
        <div class="navbar-collapse collapse" id="navbarSupportedContent">
            <ul class="navbar-nav float-left mr-auto">
                <li class="nav-item d-none d-md-block"><a class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)" data-sidebartype="mini-sidebar"><i class="mdi mdi-menu font-18"></i></a></li>

{{--                <li class="nav-item dropdown">--}}
{{--                    <a class="nav-link dropdown-toggle waves-effect waves-dark" href="#" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="font-18 mdi mdi-gmail"></i>--}}
{{--                        <div class="notify">--}}
{{--                            <span class="heartbit"></span>--}}
{{--                            <span class="point"></span>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                    <div class="dropdown-menu dropdown-menu-left mailbox animated bounceInDown" aria-labelledby="2">--}}
{{--                        <ul class="list-style-none">--}}
{{--                            <li>--}}
{{--                                <div class="drop-title border-bottom">You have 4 new messanges</div>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <div class="message-center message-body">--}}
{{--                                    <!-- Message -->--}}
{{--                                    <a href="javascript:void(0)" class="message-item">--}}
{{--                                        <span class="user-img">--}}
{{--                                            <img src="{!! asset('admin') !!}/images/users/1.jpg" alt="user" class="rounded-circle">--}}
{{--                                            <span class="profile-status online pull-right"></span>--}}
{{--                                        </span>--}}
{{--                                        <span class="mail-contnet">--}}
{{--                                                    <h5 class="message-title">Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:30 AM</span> </span>--}}
{{--                                    </a>--}}
{{--                                    <!-- Message -->--}}
{{--                                    <a href="javascript:void(0)" class="message-item">--}}
{{--                                        <span class="user-img"> <img src="{!! asset('admin') !!}/images/users/2.jpg" alt="user" class="rounded-circle"> <span class="profile-status busy pull-right"></span> </span>--}}
{{--                                        <span class="mail-contnet">--}}
{{--                                                    <h5 class="message-title">Sonu Nigam</h5> <span class="mail-desc">I've sung a song! See you at</span> <span class="time">9:10 AM</span> </span>--}}
{{--                                    </a>--}}
{{--                                    <!-- Message -->--}}
{{--                                    <a href="javascript:void(0)" class="message-item">--}}
{{--                                        <span class="user-img"> <img src="{!! asset('admin') !!}/images/users/3.jpg" alt="user" class="rounded-circle"> <span class="profile-status away pull-right"></span> </span>--}}
{{--                                        <span class="mail-contnet">--}}
{{--                                                    <h5 class="message-title">Arijit Sinh</h5> <span class="mail-desc">I am a singer!</span> <span class="time">9:08 AM</span> </span>--}}
{{--                                    </a>--}}
{{--                                    <!-- Message -->--}}
{{--                                    <a href="javascript:void(0)" class="message-item">--}}
{{--                                        <span class="user-img"> <img src="{!! asset('admin') !!}/images/users/4.jpg" alt="user" class="rounded-circle"> <span class="profile-status offline pull-right"></span> </span>--}}
{{--                                        <span class="mail-contnet">--}}
{{--                                                    <h5 class="message-title">Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span> </span>--}}
{{--                                    </a>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a class="nav-link text-center link text-dark" href="javascript:void(0);"> <b>See all Notifications</b> <i class="fa fa-angle-right"></i> </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}

            </ul>

            <ul class="navbar-nav float-right">
                <!-- ============================================================== -->
                <!-- Search -->
                <!-- ============================================================== -->
{{--                <li class="nav-item search-box">--}}
{{--                    <form class="app-search d-none d-lg-block">--}}
{{--                        <input type="text" class="form-control" placeholder="Search...">--}}
{{--                        <a href="#" class="active"><i class="fa fa-search"></i></a>--}}
{{--                    </form>--}}
{{--                </li>--}}
                <!-- ============================================================== -->
                <!-- User profile and search -->
                <!-- ============================================================== -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle waves-effect waves-dark" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if(Auth::check())
                            @if(Auth::user()->image)
                                <img src="{!! asset('media/user/'. Auth::user()->image) !!}" alt="user" class="rounded-circle" width="36">
                            @else
                                <img src="{!! asset('avatar.png') !!}" alt="user" class="rounded-circle" width="36">
                            @endif
                        @endif

                        <span class="ml-2 font-medium">{!! Auth::user()->name !!}</span><span class="fas fa-angle-down ml-2"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
                        <div class="d-flex no-block align-items-center p-3 mb-2 border-bottom">
                            <div class="">
                                @if(Auth::check())
                                    @if(Auth::user()->image)
                                        <img src="{!! asset('media/user/'. Auth::user()->image) !!}" alt="user" class="rounded" width="80">
                                    @else
                                        <img src="{!! asset('avatar.png') !!}" alt="user" class="rounded" width="80">
                                    @endif
                                @endif
                            </div>
                            <div class="ml-2">
                                <h4 class="mb-0">{!! Auth::user()->name !!}</h4>
                                <p class=" mb-0 text-muted">{!! Auth::user()->email !!}</p>
                                <a href="{!! route('user.show', Auth::user()->id) !!}" class="btn btn-sm btn-danger text-white mt-2 btn-rounded">View Profile</a>
                            </div>
                        </div>

                        <a class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" href="javascript:void(0)">
                            <i class="fa fa-power-off mr-1 ml-1"></i> Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
                <!-- ============================================================== -->
                <!-- User profile and search -->
                <!-- ============================================================== -->
            </ul>
        </div>
    </nav>
</header>
