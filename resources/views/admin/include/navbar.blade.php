<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark profile-dd" href="javascript:void(0)" aria-expanded="false">
                        @if(Auth::check())
                            @if(Auth::user()->image)
                            <img src="{!! asset('media/user/'. Auth::user()->image) !!}" class="rounded-circle ml-2" width="30">
                            @else
                            <img src="{!! asset('avatar.png') !!}" class="rounded-circle ml-2" width="30">
                            @endif
                            <span class="hide-menu">{!! Auth::user()->name !!} </span>
                        @endif
                    </a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item">
                            <a href="{!! route('user.show', Auth::user()->id) !!}" class="sidebar-link">
                                <i class="ti-user"></i>
                                <span class="hide-menu"> My Profile </span>
                            </a>
                        </li>

                        <li class="sidebar-item">
                            <a href="javascript:void(0)" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" href="javascript:void(0)" class="sidebar-link">
                                <i class="fas fa-power-off"></i>
                                <span class="hide-menu"> Logout </span>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </li>

                <li class="sidebar-item">
                    <a href="{!! route('home') !!}" class="sidebar-link">
                        <i class="mdi mdi-view-dashboard"></i>
                        <span class="hide-menu"> Dashboard </span>
                    </a>
                </li>
                <div class="devider"></div>

                <li class="sidebar-item {!! Request::is('taskman/work*') ? 'selected':'' !!}">
                    <a href="{!! route('work.index') !!}" class="sidebar-link {!! Request::is('taskman/work*') ? 'active':'' !!}">
                        <i class="fa fa-briefcase"></i>
                        <span class="hide-menu">My Works </span>
                    </a>
                </li>
                <div class="devider"></div>

                <li class="sidebar-item {!! Request::is('taskman/application*') ? 'selected':'' !!}">
                    <a href="{!! route('application.index') !!}" class="sidebar-link {!! Request::is('taskman/application*') ? 'active':'' !!}">
                        <i class="far fa-sticky-note"></i>
                        <span class="hide-menu">My Leave Application</span>
                    </a>
                </li>
                <div class="devider"></div>

                @if(Auth::user()->role == 'employee')
                <li class="sidebar-item {!! Request::is('taskman/late-report*') || Request::is('taskman/emp-monthly-late-report*') ? 'selected':'' !!}">
                    <a href="{!! route('emp.list') !!}" class="sidebar-link {!! Request::is('taskman/late-report*') || Request::is('taskman/emp-monthly-late-report*') ? 'active':'' !!}">
                        <i class="fa fa-info"></i>
                        <span class="hide-menu">My Late Report </span>
                    </a>
                </li>
                <div class="devider"></div>
                @endif

                @if(Auth::user()->role == 'admin' || Auth::user()->role == 'management')
                    <li class="sidebar-item {!! Request::is('taskman/task*') ? 'selected':'' !!}">
                        <a href="{!! route('task.index') !!}" class="sidebar-link {!! Request::is('taskman/task*') ? 'active':'' !!}">
                            <i class="fa fa-tasks"></i>
                            <span class="hide-menu"> Assign Tasks </span>
                        </a>
                    </li>
                    <div class="devider"></div>

                    <li class="sidebar-item {!! Request::is('taskman/late-report*') ? 'selected':'' !!}">
                        <a href="{!! route('late-report.index') !!}" class="sidebar-link {!! Request::is('taskman/late-report*') ? 'active':'' !!}">
                            <i class="fa fa-info"></i>
                            <span class="hide-menu">Emp. Late Report List </span>
                        </a>
                    </li>
                    <div class="devider"></div>

                    <li class="sidebar-item {!! Request::is('taskman/emp-application*') ? 'selected':'' !!}">
                        <a href="{!! route('emp.application') !!}" class="sidebar-link {!! Request::is('taskman/emp-application*') ? 'active':'' !!}">
                            <i class="far fa-sticky-note"></i>
                            <span class="hide-menu">Emp. Leave Application</span>
                        </a>
                    </li>
                    <div class="devider"></div>

                    <li class="sidebar-item {!! Request::is('taskman/exam*') ? 'selected':'' !!}">
                        <a href="{!! route('exam.index') !!}" class="sidebar-link {!! Request::is('taskman/exam*') ? 'active':'' !!}">
                            <i class="fas fa-question"></i>
                            <span class="hide-menu">Interview Exam</span>
                        </a>
                    </li>
                    <div class="devider"></div>

                    <li class="sidebar-item {!! Request::is('taskman/emp-note*') ? 'selected':'' !!}">
                        <a href="{!! route('emp-note.index') !!}" class="sidebar-link {!! Request::is('taskman/emp-note*') ? 'active':'' !!}">
                            <i class="fa fa-pencil-alt"></i>
                            <span class="hide-menu">Emp. Notes</span>
                        </a>
                    </li>
                    <div class="devider"></div>

                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="fa fa-info-circle"></i>
                            <span class="hide-menu">Reports</span>
                        </a>
                        <ul aria-expanded="false" class="collapse first-level">

                            <li class="sidebar-item {!! Request::is('taskman/monthly-late-report*') ? 'active':'' !!}">
                                <a href="{!! route('late-report.monthly') !!}" class="sidebar-link {!! Request::is('taskman/monthly-late-report*') ? 'active':'' !!}">
                                    <i class="mdi mdi-cards-variant"></i>
                                    <span class="hide-menu">Monthly Late Report </span>
                                </a>
                            </li>

                            <li class="sidebar-item {!! Request::is('taskman/yearly-leave-report*') ? 'active':'' !!}">
                                <a href="{!! url('taskman/yearly-leave-report') !!}" class="sidebar-link {!! Request::is('taskman/yearly-leave-report*') ? 'active':'' !!}">
                                    <i class="mdi mdi-cards-variant"></i>
                                    <span class="hide-menu">Yearly Leave Report </span>
                                </a>
                            </li>

                            <li class="sidebar-item {!! Request::is('taskman/salary*') ? 'active':'' !!}">
                                <a href="{!! url('taskman/salary') !!}" class="sidebar-link {!! Request::is('taskman/salary*') ? 'active':'' !!}">
                                    <i class="mdi mdi-cards-variant"></i>
                                    <span class="hide-menu">Salary Report </span>
                                </a>
                            </li>

                            <li class="sidebar-item {!! Request::is('taskman/year-end-salary*') ? 'active':'' !!}">
                                <a href="{!! url('taskman/year-end-salary') !!}" class="sidebar-link {!! Request::is('taskman/year-end-salary*') ? 'active':'' !!}">
                                    <i class="mdi mdi-cards-variant"></i>
                                    <span class="hide-menu">Year End Salary Report </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <div class="devider"></div>

                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="fa fa-cogs"></i>
                            <span class="hide-menu">Settings</span>
                        </a>
                        <ul aria-expanded="false" class="collapse first-level">

                            <li class="sidebar-item {!! Request::is('taskman/project*') ? 'active':'' !!}">
                                <a href="{!! route('project.index') !!}" class="sidebar-link {!! Request::is('taskman/project*') ? 'active':'' !!}">
                                    <i class="mdi mdi-cards-variant"></i>
                                    <span class="hide-menu"> Project </span>
                                </a>
                            </li>

                            <li class="sidebar-item {!! Request::is('taskman/user*') ? 'active':'' !!}">
                                <a href="{!! route('user.index') !!}" class="sidebar-link {!! Request::is('taskman/user*') ? 'active':'' !!}">
                                    <i class="mdi mdi-cards-variant"></i>
                                    <span class="hide-menu"> Employee </span>
                                </a>
                            </li>


                            <li class="sidebar-item {!! Request::is('taskman/leave*') ? 'selected':'' !!}">
                                <a href="{!! route('leave.index') !!}" class="sidebar-link {!! Request::is('taskman/leave*') ? 'active':'' !!}">
                                    <i class="fas fa-clipboard"></i>
                                    <span class="hide-menu">Emp. Leave </span>
                                </a>
                            </li>

                            <li class="sidebar-item {!! Request::is('taskman/leave-template*') ? 'active':'' !!}">
                                <a href="{!! route('leave-template.index') !!}" class="sidebar-link {!! Request::is('taskman/leave-template*') ? 'active':'' !!}">
                                    <i class="mdi mdi-cards-variant"></i>
                                    <span class="hide-menu">Leave Template</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <div class="devider"></div>



                @endif


            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
