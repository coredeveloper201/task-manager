@extends('admin.master')

@section('title')
    Mediusware | Task | {!! $task->name !!} | View
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Task Create</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/home') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        @if(Auth::user()->role != 'employee')
                        <li class="breadcrumb-item"><a href="{!! route('task.index') !!}">Tasks</a></li>
                        @endif
                        <li class="breadcrumb-item active" aria-current="page">View</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- End Bread crumb and right sidebar toggle -->


    <!-- Container fluid  -->

    <div class="page-content container-fluid">

        @include('admin.include.alert')

        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10 col-sm-8">
                                <h4 class="card-title">Task View</h4>
                            </div>
                            @if(Auth::user()->role != 'employee')
                            <div class="col-md-2 col-sm-4 text-right">
                                <a href="{!! route('task.index') !!}" class="btn btn-success"><i class="fa fa-arrow-left"></i> Back</a>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                            @endif
                        </div>

                        <table class="table">
                            <tr>
                                <th>Project Name</th>
                                <td>:</td>
                                <td>{!! $task->project->project_name !!}</td>
                            </tr>

                            <tr>
                                <th>Task Name</th>
                                <td>:</td>
                                <td>{!! $task->name !!}</td>
                            </tr>

                            <tr>
                                <th>Employee</th>
                                <td>:</td>
                                <td>
                                    @if(!empty($task->employees))
                                        @foreach($task->employees as $emp)
                                            <span>{!! $emp->user->name !!} </span><br>
                                        @endforeach
                                    @endif
                                </td>
                            </tr>


                            <tr>
                                <th>Task Lists</th>
                                <td>:</td>
                                <td>
                                    @if(!empty($task->taskLists))
                                        @foreach($task->taskLists as $key => $list)
                                            <p>{!! ++$key !!}. {!! $list->list_name !!}</p>
                                        @endforeach
                                    @endif
                                </td>
                            </tr>

                            <tr>
                                <th>Task Details</th>
                                <td>:</td>
                                <td>{!! $task->task_details !!}</td>
                            </tr>


                            <tr>
                                <th>Task Decoration</th>
                                <td>:</td>
                                <td>{!! date('F d, Y', strtotime($task->start_date)).' - '. date('F d, Y', strtotime($task->end_date)) !!}</td>
                            </tr>

                            <tr>
                                <th>Task Ration</th>
                                <td>:</td>
                                <td>{!! $task->ratio !!} Days</td>
                            </tr>

                            <tr>
                                <th>Task Image</th>
                                <td>:</td>
                                <td>
                                    <div class="card-columns el-element-overlay">
                                        @if(!empty($task->images))
                                            @foreach($task->images as $image)
                                                <div class="card">
                                                    <div class="el-card-item">
                                                        <div class="el-card-avatar el-overlay-1">
                                                            <a class="image-popup-vertical-fit" href="{!! asset('media/task/'. $image->image) !!}">
                                                                <img src="{!! asset('media/task/'. $image->image) !!}" class="img-fluid shadow-lg">
                                                            </a>
                                                        </div>
                                                        @if(Auth::user()->id == $task->super_vision || Auth::user()->role == 'admin')
                                                        <div class="el-card-content">
                                                            <a onclick="return confirm('Are you sure..!')" href="{!! url('taskman/media/image/'.$image->id) !!}/delete" class="btn btn-danger"> <i class="fa fa-trash"></i> Remove Image</a>
                                                        </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <hr>
                        <div class="row">
                            @if(Auth::user()->role != 'employee' )
                                <div class="col-md-5">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title">Task Review</h4>
                                            <form class="mt-3" action="{!! !empty($upReview) ? url('taskman/review/update', $upReview->id) : route('review.store') !!}" method="post">
                                                @csrf
                                                <input type="hidden" value="{!! $task->id !!}" name="task_id">
                                                
                                                <div class="form-group">
                                                    <h5>Select Rating<span class="text-danger">*</span></h5>
                                                    <div class="controls">
                                                        <select class="form-control{{ $errors->has('rating') ? ' is-invalid' : '' }}" name="rating" required data-validation-required-message="This field is required">
                                                            <option value="1" {!! !empty($upReview) && $upReview->rating == 1 ? 'selected':'' !!}>1*</option>
                                                            <option value="2" {!! !empty($upReview) && $upReview->rating == 2 ? 'selected':'' !!}>2*</option>
                                                            <option value="3" {!! !empty($upReview) && $upReview->rating == 3 ? 'selected':'' !!}>3*</option>
                                                            <option value="4" {!! !empty($upReview) && $upReview->rating == 4 ? 'selected':'' !!}>4*</option>
                                                            <option value="5" {!! !empty($upReview) && $upReview->rating == 5 ? 'selected':'' !!}>5*</option>
                                                        </select>
                                                        @if ($errors->has('rating'))
                                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('rating') }}</strong>
                                                </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <h5>Comments</h5>
                                                    <textarea type="text" class="form-control{{ $errors->has('comments') ? ' is-invalid' : '' }}" name="comments" id="nametext" placeholder="Comments">{!! old('comments', !empty($upReview) ? $upReview->comments:'') !!}</textarea>
                                                    @if ($errors->has('comments'))
                                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('comments') }}</strong>
                                                </span>
                                                    @endif
                                                </div>

                                                <div class="text-xs-right">
                                                    <button type="submit" class="btn btn-info">{!! !empty($upReview) ? 'Update':'Submit' !!}</button>
                                                    <button type="reset" class="btn btn-inverse">Reset</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            @endif
                            <div class="col-md-7">
                                <div class="table-responsive">
                                    <table id="file_export" class="table table-striped table-hover table-bordered display" style="width:100%">
                                        <thead>
                                        <tr>
                                            <th width="5%">#</th>
                                            <th>Comments</th>
                                            <th>Rating</th>
                                            <th>Date</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tablecontents">
                                        @if(!empty($reviews))
                                            @foreach($reviews as $key => $review)
                                                <tr class="row1" data-id="{{ $task->id }}">
                                                    <td>{!! ++$key !!}</td>
                                                    <td>{!! $review->comments !!}</td>
                                                    <td>
                                                        @for ($star = 1; $star <= 5; $star++)
                                                            @if ($review->rating >= $star)
                                                                <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                            @else
                                                                <li class="list-inline-item"><i class="far fa-star" aria-hidden="true"></i></li>
                                                            @endif
                                                        @endfor

                                                    </td>
                                                    <td>{!! date('F d, Y', strtotime($review->date)) !!}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!-- First Cards Row  -->
    </div>


@endsection

@section('page_js')
    <script>
        $(document).ready(function() {
            $('input[name="images[]"]').fileuploader({
                addMore: true,
                files: null,
            });
        });

    </script>
@endsection
