<!DOCTYPE html>
<html dir="ltr" lang="en">


<!-- Mirrored from wrappixel.com/ampleadmin/ampleadmin-html/ampleadmin-sidebar/index3.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 04 May 2019 05:45:27 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{!! asset('admin') !!}/images/favicon.png">
    <title>@yield('title')</title>
    <!-- This Page CSS -->
    <link href="{!! asset('admin') !!}/libs/chartist/dist/chartist.min.css" rel="stylesheet">
    <link href="{!! asset('admin') !!}/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <link href="{!! asset('admin') !!}/extra-libs/jvector/jvector.css" rel="stylesheet" />
    <link href="{!! asset('admin') !!}/extra-libs/jvector/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
    <link href="{!! asset('admin') !!}/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{!! asset('admin') !!}/libs/summernote/dist/summernote-bs4.css">
    <link rel="stylesheet" type="text/css" href="{!! asset('admin') !!}/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" type="text/css" href="{!! asset('admin') !!}/libs/dropzone/dist/min/dropzone.min.css">
    <link rel="stylesheet" type="text/css" href="{!! asset('admin') !!}/libs/select2/dist/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="{!! asset('admin') !!}/image-upload/jquery.fileuploader.min.css">
    <link rel="stylesheet" type="text/css" href="{!! asset('admin') !!}/libs/magnific-popup/dist/magnific-popup.css">
    <!-- needed css -->
    <link rel="stylesheet" href="{{asset('examCSS/exam.css')}}">
    <link href="{!! asset('admin') !!}/dist/css/style.min.css" rel="stylesheet">
    @yield('page_css')
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- Preloader - style you can find in spinners.css -->

<div class="preloader">
    <div class="lds-ripple">
        <div class="lds-pos"></div>
        <div class="lds-pos"></div>
    </div>
</div>

<!-- Main wrapper - style you can find in pages.scss -->

<div id="main-wrapper">

    <!-- Topbar header -->
    @include('admin.include.header')
    <!-- End -->


    <!-- Left Sidebar -->
    @include('admin.include.navbar')
    <!-- End Left Sidebar  -->

    <!-- Page wrapper  -->

    <div class="page-wrapper">

        <!-- Main Content -->
        @yield('content')
        <!-- End Main Content -->

        <!-- footer -->
        @include('admin.include.footer')
        <!-- End footer -->
    </div>
</div>

<!-- All Jquery -->

<script src="{!! asset('admin') !!}/libs/jquery/dist/jquery.min.js"></script>

<!-- apps -->
<script src="{!! asset('admin') !!}/dist/js/app.min.js"></script>
<script src="{!! asset('admin') !!}/dist/js/app.init.js"></script>
<script src="{!! asset('admin') !!}/dist/js/app-style-switcher.js"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{!! asset('admin') !!}/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
<script src="{!! asset('admin') !!}/extra-libs/sparkline/sparkline.js"></script>
<!--Wave Effects -->
<script src="{!! asset('admin') !!}/dist/js/waves.js"></script>
<!--Menu sidebar -->
<script src="{!! asset('admin') !!}/dist/js/sidebarmenu.js"></script>
<!--Custom JavaScript -->
<script src="{!! asset('admin') !!}/dist/js/custom.min.js"></script>
<!-- This Page JS -->
<script src="{!! asset('admin') !!}/dist/js/pages/dashboards/dashboard3.js"></script>
<script src="{!! asset('admin') !!}/extra-libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="{!! asset('admin') !!}/dist/js/pages/datatable/datatable-advanced.init.js"></script>
<script src="{!! asset('admin') !!}/extra-libs/jqbootstrapvalidation/validation.js"></script>
<script src="{!! asset('admin') !!}/libs/summernote/dist/summernote-bs4.min.js"></script>
<script src="{!! asset('admin') !!}/dist/js/jquery.ui.js"></script>

<!-- Bootstrap tether Core JavaScript -->
<script src="{!! asset('admin') !!}/libs/popper.js/dist/umd/popper.min.js"></script>
<script src="{!! asset('admin') !!}/libs/bootstrap/dist/js/bootstrap.min.js"></script>

<script src="{!! asset('admin') !!}/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="{!! asset('admin') !!}/libs/jquery.repeater/jquery.repeater.min.js"></script>
<script src="{!! asset('admin') !!}/extra-libs/jquery.repeater/repeater-init.js"></script>
<script src="{!! asset('admin') !!}/extra-libs/jquery.repeater/dff.js"></script>
<script src="{!! asset('admin') !!}/libs/select2/dist/js/select2.full.min.js"></script>
<script src="{!! asset('admin') !!}/libs/select2/dist/js/select2.min.js"></script>
<script src="{!! asset('admin') !!}/dist/js/pages/forms/select2/select2.init.js"></script>
<script src="{!! asset('admin') !!}/image-upload/jquery.fileuploader.min.js"></script>
<script src="{!! asset('admin') !!}/libs/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
<script src="{!! asset('admin') !!}/libs/magnific-popup/meg.init.js"></script>

@yield('page_js')
<script>
    ! function(window, document, $) {
        "use strict";
        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    }(window, document, jQuery);

    setTimeout(function(){ $(".alert").hide(); }, 4000);

    $('.summernote').summernote({
        height: 300, // set editor height
        minHeight: null, // set minimum height of editor
        maxHeight: null, // set maximum height of editor
        focus: false // set focus to editable area after initializing summernote
    });

    jQuery('.mydatepicker, #datepicker, .input-group.date').datepicker();
    jQuery('.datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd'
    });
</script>

<script src="https://www.gstatic.com/firebasejs/5.5.4/firebase.js"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function registerFcmToken(token){
        var param = {token: token};
        console.log(param)
        $.ajax({
            url: "{{env('APP_URL')}}/taskman/users/registerFcmToken",
            type: 'post',
            data: param,
            success: function(res){
                console.log(res)
            }
        });
    }
    // Initialize Firebase
    var config = {
        apiKey: "AIzaSyCI887TRD-x8JetVdavH_AW1hmwbrYxaFs",
        authDomain: "laravel-test-6d8a7.firebaseapp.com",
        databaseURL: "https://laravel-test-6d8a7.firebaseio.com",
        projectId: "laravel-test-6d8a7",
        storageBucket: "laravel-test-6d8a7.appspot.com",
        messagingSenderId: "816860377990"
    };
    firebase.initializeApp(config);
    navigator.serviceWorker.register("{{env('APP_URL')}}/firebase-messaging-sw.js").then(function(){
        const messaging = firebase.messaging();
        messaging.requestPermission().then(function () {
            return messaging.getToken();
        }).then(function (token) {
            registerFcmToken(token);
        }).catch(function (err) {
        });

        messaging.onMessage(function(payload) {
            console.log("Message received. ", payload);
            //https://developer.mozilla.org/en-US/docs/Web/API/notification/Notification
            navigator.serviceWorker.ready.then(function(registration) {
                registration.showNotification(payload.notification.title,{
                    tag : payload.notification.tag,
                    body : payload.notification.body,
                    icon : payload.notification.icon,
                    image : payload.notification.image,
                    vibrate : [500,110,500,110,450,110,200,110,170,40,450,110,200,110,170,40,500],
                    sound : 'https://notificationsounds.com/soundfiles/dd458505749b2941217ddd59394240e8/file-sounds-1111-to-the-point.ogg'
                });
            });
        });
    });
</script>

<!-- Mirrored from wrappixel.com/ampleadmin/ampleadmin-html/ampleadmin-sidebar/index3.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 04 May 2019 05:45:30 GMT -->
</body>
</html>
