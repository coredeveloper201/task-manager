@extends('admin.master')

@section('title')
    Mediusware | {!! $user->name !!}
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">user Create</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/home') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('user.index') !!}">Users</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Create</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- End Bread crumb and right sidebar toggle -->


    <!-- Container fluid  -->

    <div class="page-content container-fluid">

        @include('admin.include.alert')

        <div class="row">
            <!-- Column -->
            <div class="col-lg-4 col-xlg-3 col-md-5">
                <div class="card">
                    <div class="card-body">
                        <center class="mt-4">
                            @if(!empty($user))
                                @if($user->image)
                                    <img src="{!! asset('media/user/'. $user->image) !!}" class="rounded-circle" width="150">
                                @else
                                    <img src="{!! asset('avatar.png') !!}" class="rounded-circle" width="150">
                                @endif
                            @endif


                            <h4 class="card-title mt-2">{!! $user->name !!}</h4>
                            <h6 class="card-subtitle">
                                @if($user->role == 'employee')
                                    Employee
                                @endif
                            </h6>

                        </center>
                    </div>
                    <div>
                        <hr> </div>
                    <div class="card-body"> <small class="text-muted">Email address </small>
                        <h6>{!! $user->email !!}</h6> <small class="text-muted pt-4 db">Phone</small>
                        <h6>{!! $user->phone !!}</h6> <small class="text-muted pt-4 db">About</small>
                        <h6 class="text-justify">{!! $user->about !!}</h6>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-8 col-xlg-9 col-md-7">
                <div class="card">
                    <!-- Tabs -->
                    <ul class="nav nav-pills custom-pills"  id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#timeline" aria-controls="timeline" role="tab" data-toggle="tab" aria-selected="true">Timeline</a>
                        </li>

                        @if($user->id == Auth::user()->id)
                        <li class="nav-item">
                            <a class="nav-link" href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a>
                        </li>
                        @endif
                    </ul>
                    <!-- Tabs -->
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" role="tabpanel" id="timeline">
                            <div class="card-body">
                                <div class="steamline mt-0">
                                    @if(!empty($tasks))
                                        @foreach($tasks as $task)
                                        <div class="sl-item">
                                            <div class="sl-left">
                                                @if($task->supervisor->image)
                                                    <img src="{!! asset('media/user/'. $task->supervisor->image) !!}" class="rounded-circle">
                                                @else
                                                    <img src="{!! asset('avatar.png') !!}" class="rounded-circle">
                                                @endif
                                            </div>
                                            <div class="sl-right">
                                                <div>
                                                    <a href="javascript:void(0)" class="link">{!! $task->supervisor->name !!}</a>
                                                    <span class="sl-date">{!! date('F d, Y | h:i a', strtotime($task->created_at) + 6*3600) !!}</span>
                                                    <p><a href="javascript:void(0)"> {!! $task->name !!} ({!! isset($task->project->project_name) ? $task->project->project_name:'N/A' !!})</a></p>
                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12 mb-3">
                                                            @if(count($task->taskLists) > 0)
                                                            <h5>Task List:</h5>
                                                            <ul>
                                                                @foreach($task->taskLists as $list)
                                                                <li>{!! $list->list_name !!} </li>
                                                                @endforeach
                                                            </ul>
                                                            @endif
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            @if(count($task->works)>0)

                                            <div class="sl-right">
                                                <div class="row">
                                                    @foreach($task->works as $work)
                                                        @if($user->id == $work->employee_id)
                                                            <div class="col-lg-12 col-md-12 mb-3">
                                                                <h4>Work Details</h4>
                                                                <p>{!! $work->done_task_details !!}</p>
                                                            </div>

                                                            <div class="col-lg-12 col-md-12 mb-3">
                                                                @if($work->note)
                                                                <p><span style="font-size: 15px;" class="badge badge-pill badge-info">Note:</span> {!! $work->note !!}</p>
                                                                @endif
                                                                <p>
                                                                    <span>Work Status:</span>
                                                                    @if($work->status == 'pending')
                                                                        <span class="badge badge-pill badge-warning">Pending</span>
                                                                    @elseif($work->status == 'working')
                                                                        <span class="badge badge-pill badge-primary">Working</span>
                                                                    @elseif($work->status == 'complete')
                                                                        <span class="badge badge-pill badge-success">Complete</span>
                                                                    @else
                                                                        <span class="badge badge-pill badge-danger">Rejected</span>
                                                                    @endif
                                                                </p>
                                                                @php
                                                                    $sum = 0;
                                                                    if (count($work->reviews) > 0){
                                                                        foreach ($work->reviews as $rating){
                                                                            $sum+= $rating->rating;
                                                                        }
                                                                        $resRating = $sum / count($work->reviews);
                                                                    }else{
                                                                        $resRating = 0;
                                                                    }
                                                                @endphp
                                                                <p>
                                                                    <span>Work Rating:</span>
                                                                    @for ($star = 1; $star <= 5; $star++)
                                                                        @if ($resRating >= $star)
                                                                            <i class="fa fa-star text-warning"></i>
                                                                        @else
                                                                            <i class="far fa-star text-warning" aria-hidden="true"></i>
                                                                        @endif
                                                                    @endfor
                                                                </p>
                                                            </div>
                                                            <div class="card-columns el-element-overlay">
                                                                @if(!empty($work->images))
                                                                    @foreach($work->images as $image)
                                                                        <div class="card">
                                                                            <div class="el-card-item">
                                                                                <div class="el-card-avatar el-overlay-1">
                                                                                    <a class="image-popup-vertical-fit" href="{!! asset('media/task/'. $image->image) !!}">
                                                                                        <img src="{!! asset('media/task/'. $image->image) !!}" class="img-fluid shadow-lg">
                                                                                    </a>
                                                                                </div>
                                                                                @if(Auth::user()->role == 'employee')
                                                                                    <div class="el-card-content">
                                                                                        <a onclick="return confirm('Are you sure..!')" href="{!! url('taskman/media/image/'.$image->id) !!}/delete" class="btn btn-danger"> <i class="fa fa-trash"></i> Remove Image</a>
                                                                                    </div>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    @endforeach
                                                                @endif
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                        @endforeach
                                            {!! $tasks->links() !!}
                                    @endif

                                </div>
                            </div>
                        </div>

                        @if($user->id == Auth::user()->id)
                        <div class="tab-pane fade" role="tabpanel" id="profile">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-7 border-right">
                                        <div class="page-header border-bottom mb-3">
                                            <h3>Profile Update</h3>
                                        </div>
                                        <form class="form-horizontal form-material" method="post" action="{!! route('user.update', $user->id) !!}" novalidate enctype="multipart/form-data">
                                            @method('PATCH')
                                            @csrf
                                            <div class="form-group">
                                                <label class="col-md-12">Full Name</label>
                                                <div class="col-md-12">
                                                    <input type="text" placeholder="Enter your name" name="name" value="{!! $user->name !!}" class="form-control form-control-line">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-12">Phone No</label>
                                                <div class="col-md-12">
                                                    <input type="text" placeholder="Enter your phone number" name="phone" value="{!! $user->phone !!}" class="form-control form-control-line">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-12">About</label>
                                                <div class="col-md-12">
                                                    <textarea rows="5" placeholder="Enter your about" name="about" class="form-control form-control-line">{!! $user->about !!}</textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="example-email" class="col-md-12">Image (300x300)</label>
                                                <div class="col-md-12">
                                                    <input type="file" name="image" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }} form-control-line" accept="image/*">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button class="btn btn-success">Update Profile</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="page-header border-bottom mb-3">
                                                    <h3>Reset Password</h3>
                                                </div>

                                                <form class="form-horizontal form-material" method="post" action="{!! url('taskman/user/reset-password', $user->id) !!}">
                                                    @csrf
                                                    <div class="form-group">
                                                        <label class="col-md-12">Old Password</label>
                                                        <div class="col-md-12">
                                                            <input type="password" placeholder="Enter Old Password" name="old_password" value="{!! old('old_password') !!}" class="form-control form-control-line">
                                                            @if ($errors->has('old_password'))
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $errors->first('old_password') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-12">New Password</label>
                                                        <div class="col-md-12">
                                                            <input type="password" placeholder="Enter New Password" name="password"  value="{!! old('password') !!}" class="form-control form-control-line">
                                                            @if ($errors->has('password'))
                                                                <small class="text-danger">{{ $errors->first('password') }}</small>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-12">Confirm Password</label>
                                                        <div class="col-md-12">
                                                            <input type="password" placeholder="Enter Confirm Password" value="{!! old('password_confirmation') !!}" name="password_confirmation" class="form-control form-control-line">
                                                            @if ($errors->has('password_confirmation'))
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <button class="btn btn-success">Update</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                            <div class="col-md-12" style="margin-top: 25px;">
                                                <div class="page-header border-bottom mb-3">
                                                    <h3>Reset Email</h3>
                                                </div>

                                                <form class="form-horizontal form-material" method="post" action="{!! url('taskman/user/reset-email', $user->id) !!}">
                                                    @csrf
                                                    <div class="form-group">
                                                        <label class="col-md-12">New Email</label>
                                                        <div class="col-md-12">
                                                            <input type="email" placeholder="Enter New Email" value="{!! old('email') !!}" name="email" class="form-control form-control-line">
                                                            @if ($errors->has('email'))
                                                                <small class="text-danger">{{ $errors->first('email') }}</small>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <button class="btn btn-success">Update</button>
                                                        </div>
                                                    </div>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>

        <!-- First Cards Row  -->
    </div>


@endsection

@section('page_js')
<script>
    $(function() {

        $('a[data-toggle="tab"]').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });

        $('a[data-toggle="tab"]').on("shown.bs.tab", function (e) {
            var id = $(e.target).attr("href");
            localStorage.setItem('selectedTab', id)
        });

        var selectedTab = localStorage.getItem('selectedTab');
        if (selectedTab != null) {
            $('a[data-toggle="tab"][href="' + selectedTab + '"]').tab('show');

        }
    });


</script>
@endsection
