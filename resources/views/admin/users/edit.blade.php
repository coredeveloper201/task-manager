@extends('admin.master')

@section('title')
    Mediusware | user Edit | {!! $user->name !!}
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">User Edit | {!! $user->name !!}</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/home') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{!! url('/taskman/user') !!}">Users</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <!-- End Bread crumb and right sidebar toggle -->

    <!-- Container fluid  -->

    <div class="page-content container-fluid">

        @include('admin.include.alert')

        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-10">
                                <h4 class="card-title">User Update</h4>
                            </div>
                            <div class="col-2 text-right">
                                <a href="{!! route('user.index') !!}" class="btn btn-success"><i class="fa fa-arrow-left"></i> Back</a>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-7 col-md-7 col-xs-12">
                                <form class="" method="post" action="{!! route('user.update', $user->id) !!}" novalidate enctype="multipart/form-data">
                                    @method('PATCH')
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <h5>Select Role<span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <select class="form-control{{ $errors->has('role') ? ' is-invalid' : '' }}" name="role">
                                                    <option value="management" {!! $user->role == 'management' ? 'selected':'' !!}>Management</option>
                                                    <option value="employee" {!! $user->role == 'employee' ? 'selected':'' !!}>Employee</option>
                                                </select>
                                                @if ($errors->has('role'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('role') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-12 form-group">
                                            <h5>Name<span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input type="text" name="name" value="{!! old('name', $user->name) !!}" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                                @if ($errors->has('name'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-12 form-group">
                                            <h5>Email<span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input type="email" name="email" value="{!! old('email', $user->email) !!}" disabled class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                                @if ($errors->has('email'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-12 form-group">
                                            <h5>Salary<span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input type="number" name="salary" value="{!! old('salary', $user->salary) !!}" class="form-control{{ $errors->has('salary') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                                @if ($errors->has('salary'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('salary') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-12 form-group">
                                            <h5>Joining Date</h5>
                                            <div class="controls">
                                                <input type="text" name="joining_date" value="{!! old('joining_date', $user->joining_date) !!}" class="datepicker-autoclose form-control{{ $errors->has('joining_date') ? ' is-invalid' : '' }}">
                                                @if ($errors->has('joining_date'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('joining_date') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-12 form-group">
                                            <h5>Permanent Date</h5>
                                            <div class="controls">
                                                <input type="text" name="permanent_date" value="{!! old('permanent_date', $user->permanent_date) !!}" class="datepicker-autoclose form-control{{ $errors->has('permanent_date') ? ' is-invalid' : '' }}">
                                                @if ($errors->has('permanent_date'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('permanent_date') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-12 form-group">
                                            <h5>Image<span class="text-danger">*</span> (300x300)</h5>
                                            <div class="controls">
                                                <input type="file" name="image" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}">
                                                <br>
                                                <img src="{!! asset('media/user/'. $user->image) !!}" width="100">
                                                @if ($errors->has('image'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('image') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-12 form-group validate">
                                            <h5>Status <span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <fieldset>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" {!! $user->status == 'active' ? 'checked':'' !!} name="status" value="active" required="" id="status1" class="custom-control-input" aria-invalid="false">
                                                        <label class="custom-control-label" for="status1">Active</label>
                                                    </div>
                                                </fieldset>
                                                <fieldset>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" {!! $user->status == 'deactivate' ? 'checked':'' !!}  name="status" value="deactivate" id="status" class="custom-control-input" aria-invalid="false">
                                                        <label class="custom-control-label" for="status">Deactivate</label>
                                                    </div>
                                                </fieldset>
                                                @if ($errors->has('status'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('status') }}</strong>
                                                    </span>
                                                @endif
                                                <div class="help-block"></div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>

                                    <div class="col-md-12 text-xs-right">
                                        <button type="submit" class="btn btn-info">Update</button>
                                        <button type="reset" class="btn btn-inverse">Reset</button>
                                    </div>
                                </form>
                            </div>

                            <div class="col-lg-5 col-md-5 col-xs-12">
                                <form class="" method="post" action="{!! route('user.update-password', $user->id) !!}" novalidate>
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <h5>Password<span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input type="password" name="password" value="{!! old('password') !!}" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                                @if ($errors->has('password'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-12 text-xs-right">
                                            <button type="submit" class="btn btn-info">Update</button>
                                            <button type="reset" class="btn btn-inverse">Reset</button>
                                        </div>
                                        <hr>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!-- First Cards Row  -->
    </div>


@endsection
@section('page_js')

@endsection
