@extends('admin.master')

@section('title')
    Mediusware | Dashboard
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Dashboard</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/home') !!}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <!-- End Bread crumb and right sidebar toggle -->


    <!-- Container fluid  -->

    <div class="page-content container-fluid">
        <!-- First Cards Row  -->
        <div class="row">
            <div class="col-md-6 col-lg-3">
                <div class="card shadow-box">
                    <div class="card-body">
                        <h5 class="card-title text-uppercase">All Tasks</h5>
                        <div class="d-flex align-items-center mb-2 mt-4">
                            <h2 class="mb-0 display-5"><i class="fa fa-tasks text-info"></i></h2>
                            <div class="ml-auto">
                                <h2 class="mb-0 display-6"><span class="font-normal">{!! count($tasks) !!}</span></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-3">
                <div class="card shadow-box">
                    <div class="card-body">
                        <h5 class="card-title text-uppercase">Today Assign Tasks</h5>
                        <div class="d-flex align-items-center mb-2 mt-4">
                            <h2 class="mb-0 display-5"><i class="fa fa-tasks text-info"></i></h2>
                            <div class="ml-auto">
                                <h2 class="mb-0 display-6"><span class="font-normal">{!! count($todayTasks) !!}</span></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-3">
                <div class="card shadow-box">
                    <div class="card-body">
                        <h5 class="card-title text-uppercase">All Complete Work</h5>
                        <div class="d-flex align-items-center mb-2 mt-4">
                            <h2 class="mb-0 display-5"><i class="fa fa-check-circle text-success"></i></h2>
                            <div class="ml-auto">
                                <h2 class="mb-0 display-6"><span class="font-normal">{!! count($completeWorks) !!}</span></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-3">
                <div class="card shadow-box">
                    <div class="card-body">
                        <h5 class="card-title text-uppercase">Deactivate Tasks</h5>
                        <div class="d-flex align-items-center mb-2 mt-4">
                            <h2 class="mb-0 display-5"><i class="fa fa-window-close text-danger"></i></h2>
                            <div class="ml-auto">
                                <h2 class="mb-0 display-6"><span class="font-normal">{!! count($deactivateTasks) !!}</span></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="card shadow-box">
                    <div class="card-body">
                        <h5 class="card-title text-uppercase">Complete Tasks</h5>
                        <div class="d-flex align-items-center mb-2 mt-4">
                            <h2 class="mb-0 display-5"><i class="fa fa-check-circle text-success"></i></h2>
                            <div class="ml-auto">
                                <h2 class="mb-0 display-6"><span class="font-normal">{!! count($completeTasks) !!}</span></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-3">
                <div class="card shadow-box">
                    <div class="card-body">
                        <h5 class="card-title text-uppercase">All Works</h5>
                        <div class="d-flex align-items-center mb-2 mt-4">
                            <h2 class="mb-0 display-5"><i class="fa fa-briefcase text-warning"></i></h2>
                            <div class="ml-auto">
                                <h2 class="mb-0 display-6"><span class="font-normal">{!! count($works) !!}</span></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-3">
                <div class="card shadow-box">
                    <div class="card-body">
                        <h5 class="card-title text-uppercase">Today Works</h5>
                        <div class="d-flex align-items-center mb-2 mt-4">
                            <h2 class="mb-0 display-5"><i class="fa fa-briefcase text-warning"></i></h2>
                            <div class="ml-auto">
                                <h2 class="mb-0 display-6"><span class="font-normal">{!! count($todayWorks) !!}</span></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-3">
                <div class="card shadow-box">
                    <div class="card-body">
                        <h5 class="card-title text-uppercase">Today Complete Works</h5>
                        <div class="d-flex align-items-center mb-2 mt-4">
                            <h2 class="mb-0 display-5"><i class="fa fa-briefcase text-warning"></i></h2>
                            <div class="ml-auto">
                                <h2 class="mb-0 display-6"><span class="font-normal">{!! count($todayCompleteWorks) !!}</span></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-md-6">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="page-header border-bottom mb-3">
                            <h3>Today assign task list</h3>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-bordered display" style="width:100%; height:300px;">
                                <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th>Pro. Name</th>
                                    <th>Task Name</th>
                                    <th>Emp. Name</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Ratio</th>
                                    <th width="10%">Action</th>
                                </tr>
                                </thead>
                                <tbody id="tablecontents">
                                @if(count($todayTasks)>0)
                                    @foreach($todayTasks as $key => $todayTask)
                                        <tr class="row1" data-id="{{ $todayTask->id }}">
                                            <td>{!! ++$key !!}</td>
                                            <td>{!! isset($todayTask->project->project_name) ? $todayTask->project->project_name:'' !!}</td>
                                            <td>{!! $todayTask->name !!}</td>
                                            <td>
                                                @if(!empty($todayTask->employees))
                                                    @foreach($todayTask->employees as $emp)
                                                        <span>{!! $emp->user->name !!} </span><br>
                                                    @endforeach
                                                @endif
                                            </td>
                                            <td>{!! date('F d, Y', strtotime($todayTask->start_date)) !!}</td>
                                            <td>{!! date('F d, Y', strtotime($todayTask->end_date)) !!}</td>
                                            <td>{!! $todayTask->ratio !!} Days</td>
                                            <td>
                                                <a href="{!! url('taskman/task/'.$todayTask->id) !!}" class="btn btn-success btn-circle"><i class="fa fa-eye"></i> </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="8" align="center">Data not found!</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="page-header border-bottom mb-3">
                            <h3>Today work list</h3>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-bordered display " style="width:100%; height:300px;">
                                <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th>Emp.Name</th>
                                    <th>Task Name</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Status</th>
                                    <th width="10%">Action</th>
                                </tr>
                                </thead>
                                <tbody id="tablecontents">
                                @if(count($todayWorks)>0)
                                    @foreach($todayWorks as $key => $todayWork)
                                        <tr class="row1" data-id="{{ $todayWork->id }}">
                                            <td>{!! ++$key !!}</td>
                                            <td>{!! isset($todayWork->user->name) ? $todayWork->user->name: '' !!}</td>
                                            <td>{!! $todayWork->task->name !!}</td>
                                            <td>{!! date('F d, Y', strtotime($todayWork->task->start_date)) !!}</td>
                                            <td>{!! date('F d, Y', strtotime($todayWork->task->end_date)) !!}</td>
                                            <td>
                                                @if($todayWork->status == 'pending')
                                                    <span class="badge badge-pill badge-warning">Pending</span>
                                                @elseif($todayWork->status == 'working')
                                                    <span class="badge badge-pill badge-primary">Working</span>
                                                @elseif($todayWork->status == 'complete')
                                                    <span class="badge badge-pill badge-success">Complete</span>
                                                @else
                                                    <span class="badge badge-pill badge-danger">Rejected</span>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{!! url('taskman/work/'.$todayWork->id) !!}" class="btn btn-success btn-circle"><i class="fa fa-eye"></i> </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="8" align="center">Data not found!</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            @if(!empty($leave))
            <div class="col-md-6">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="page-header border-bottom mb-3">
                            <h3>Emp. Leave report ({!! date('Y') !!})</h3>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-bordered display" style="width:100%; height:300px;">
                                <thead>
                                <tr>
                                    <th>Type</th>
                                    <th>Casual</th>
                                    <th>Sick</th>
                                </tr>
                                </thead>
                                <tbody id="tablecontents">
                                    <tr>
                                        <th>Total</th>
                                        <td>{!! $leave->casual_leave !!}</td>
                                        <td>{!! $leave->sick_leave !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Leave</th>
                                        <td>{!! $leave->t_casual !!}</td>
                                        <td>{!! $leave->t_sick !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Remain</th>
                                        <td>{!! ($leave->casual_leave-$leave->t_casual) !!}</td>
                                        <td>{!! ($leave->sick_leave-$leave->t_sick) !!}</td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            @endif

            <div class="col-md-6">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="page-header border-bottom mb-3">
                            <h3>Emp. Late report ({!! date('F, Y', strtotime($month.$year)) !!})</h3>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-bordered display" style="width:100%; height:300px;">
                                <thead>
                                <tr>
                                    <th width="8%">Avatar</th>
                                    <th>Name</th>
                                    <th>10 to 11</th>
                                    <th>11 to 12</th>
                                    <th>After 12</th>
                                </tr>
                                </thead>
                                <tbody id="tablecontents">
                                @if(count($data)>0)
                                    @foreach($data as $user)
                                        <tr>
                                            <td class="text-center">
                                                @if($user->image)
                                                    <img src="{!! asset('media/user/'.$user->image) !!}" width="50">
                                                @else
                                                    <img src="{!! asset('avatar.png') !!}" width="50">
                                                @endif
                                            </td>
                                            <td>{!! $user->name !!}</td>
                                            <td>
                                                @if($user->in_time_10 == 1)
                                                    {!! $user->in_time_10. ' Day'  !!}
                                                @else
                                                    {!! isset($user->in_time_10) ? $user->in_time_10.' Days':'0 Day'  !!}
                                                @endif
                                            </td>
                                            <td>
                                                @if($user->in_time_11 == 1)
                                                    {!! $user->in_time_11.' Day'  !!}
                                                @else
                                                    {!! isset($user->in_time_11) ? $user->in_time_11.' Days':'0 Day'  !!}
                                                @endif
                                            </td>
                                            <td>
                                                @if($user->in_time_12 == 1)
                                                    {!! $user->in_time_12.' Day'  !!}
                                                @else
                                                    {!! isset($user->in_time_12) ? $user->in_time_12.' Days':'0 Day'  !!}
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="8" align="center">Data not found!</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>

@endsection
