@extends('admin.master')

@section('title')
    Mediusware | Leave
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Users</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/home') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Users</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- End Bread crumb and right sidebar toggle -->

    <!-- Container fluid  -->
    <div class="page-content container-fluid">

    @include('admin.include.alert')
    <!-- First Cards Row  -->
        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10">
                                <h4 class="card-title">All Users</h4>
                            </div>
                            <div class="col-md-2 text-right">
                                <a href="{!! route('user.create') !!}" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add New</a>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>

                        <form class="" method="post" action="{!! url('taskman/leave') !!}" novalidate enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <h5>Select Year</h5>
                                        <div class="controls">
                                            <select class="form-control" name="year" required data-validation-required-message="This field is required">
                                                @for($i = date('Y') ; $i >= 2019; $i--)
                                                    <option value="{!! $i !!}">
                                                        {!! $i !!}
                                                    </option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <h5>Casual Leave</h5>
                                        <div class="controls">
                                            <input type="number" max="15" min="0" id="input_casual_leave" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <h5>Sick Leave</h5>
                                        <div class="controls">
                                            <input type="number" max="12" min="0" id="input_sick_leave" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-bordered display" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th width="8%">Avatar</th>
                                        <th>Name</th>
                                        <th>Casual Leave</th>
                                        <th>Sick Leave</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tablecontents">
                                    @foreach($users as $key => $user)
                                        <input type="hidden" name="user_id[]" value="{!! $user->id !!}">
                                        <tr>
                                            <td>{!! ++$key !!}</td>
                                            <td class="text-center">
                                                @if($user->image)
                                                    <img src="{!! asset('media/user/'.$user->image) !!}" width="50">
                                                @else
                                                    <img src="{!! asset('avatar.png') !!}" width="50">
                                                @endif
                                            </td>
                                            <td>{!! $user->name !!}</td>

                                            <td>
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <input type="number" max="15" min="0" name="casual_leave[]" value="{!! old('casual_leave') !!}" class="casual_leave form-control{{ $errors->has('casual_leave') ? ' is-invalid' : '' }}">
                                                    </div>
                                                </div>
                                            </td>

                                            <td>
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <input type="number" max="12" min="0" name="sick_leave[]" value="{!! old('sick_leave') !!}" class="sick_leave form-control{{ $errors->has('sick_leave') ? ' is-invalid' : '' }}">
                                                    </div>
                                                </div>
                                            </td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <button type="submit" class="btn btn-info">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>




@endsection

@section('page_js')
    <script type="text/javascript">
        $('input#input_casual_leave').bind("change keyup input",function() {
            var cl = $('#input_casual_leave').val();
            $('.casual_leave').val(cl);
        });

        $('input#input_sick_leave').bind("change keyup input",function() {
            var sl = $('#input_sick_leave').val();
            $('.sick_leave').val(sl);
        });

    </script>
@endsection
