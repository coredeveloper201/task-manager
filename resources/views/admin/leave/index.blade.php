@extends('admin.master')

@section('title')
    Mediusware | Leave
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Leaves</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/home') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Users</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- End Bread crumb and right sidebar toggle -->

    <!-- Container fluid  -->
    <div class="page-content container-fluid">

    @include('admin.include.alert')
        <!-- First Cards Row  -->
        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10">
                                <h4 class="card-title">All Leaves</h4>
                            </div>
                            <div class="col-md-2 text-right">
                                <a href="{!! route('leave.create') !!}" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add New</a>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table id="row_create_call" class="table table-striped table-hover table-bordered display" style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Yar</th>
                                    <th>Total Users</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($leaves as $key => $leave)
                                    <tr>
                                        <td>{!! ++$key !!}</td>
                                        <td>{!! $leave->year !!}</td>
                                        <td>{!! $leave->total_user !!}</td>
                                        <td>
                                            <a href="{!! url('taskman/leave/'.$leave->year) !!}/edit" class="btn btn-info btn-circle"><i class="fa fa-edit"></i> </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>




@endsection

@section('page_js')
    <script type="text/javascript">
        $('input#input_casual_leave').bind("change keyup input",function() {
            var cl = $('#input_casual_leave').val();
            $('.casual_leave').val(cl);
        });

        $('input#input_sick_leave').bind("change keyup input",function() {
            var sl = $('#input_sick_leave').val();
            $('.sick_leave').val(sl);
        });

    </script>
@endsection
