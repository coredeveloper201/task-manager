@extends('admin.master')

@section('title')
    Mediusware | Yearly Leave Report
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Yearly Leave Report</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Yearly Leave Report</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- End Bread crumb and right sidebar toggle -->

    <!-- Container fluid  -->
    <div class="page-content container-fluid">

    @include('admin.include.alert')
        <!-- First Cards Row  -->
        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10">
                                <h4 class="card-title">Yearly Leave Report</h4>
                                <hr>
                            </div>
                        </div>

                        <form action="{!! url('taskman/yearly-leave-report') !!}" method="get">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <h5>Select Year</h5>
                                        <div class="controls">
                                            <select class="form-control" name="year" required data-validation-required-message="This field is required">
                                                @for($i = date('Y') ; $i >= 2019; $i--)
                                                    <option value="{!! $i !!}" {!! $year == $i ? 'selected':'' !!}>
                                                        {!! $i !!}
                                                    </option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <br>
                                        <div class="controls">
                                            <button type="submit" class="btn btn-info">Go</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive">
                            <table id="row_create_call" class="table table-striped table-hover table-bordered display" style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th class="text-center">Emp.Name</th>
                                    <th class="text-center" colspan="2">Leaves</th>
                                    <th class="text-center" colspan="2">Use</th>
                                    <th class="text-center" colspan="2">Remaining</th>
                                </tr>

                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>Casual Leave</td>
                                    <td>Sick Leave</td>

                                    <td>Casual Leave</td>
                                    <td>Sick Leave</td>

                                    <td>Casual Leave</td>
                                    <td>Sick Leave</td>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($leaves as $key => $leave)
                                    @php($remainC = ($leave->casual_leave - $leave->t_casual))
                                    @php($remainS = ($leave->sick_leave - $leave->t_sick))
                                    <tr>
                                        <td>{!! ++$key !!}</td>
                                        <td>{!! $leave->user->name !!}</td>
                                        <td>{!! $leave->casual_leave !!}</td>
                                        <td>{!! $leave->sick_leave !!}</td>
                                        <td>{!! $leave->t_casual !!}</td>
                                        <td>{!! $leave->t_sick !!}</td>
                                        <td>
                                            {!! $remainC !!}
                                            {!! ($remainS < 0)?'('.str_replace('-', '', $remainS).') <strong>'. ($remainC+$remainS).'</strong>':'' !!}
                                        </td>
                                        <td>
                                            {!! $remainS !!}
                                            {!! ($remainC < 0)?'('.str_replace('-', '', $remainC).') <strong>'. ($remainS+$remainC).'</strong>':'' !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>




@endsection

@section('page_js')
    <script type="text/javascript">
        $('input#input_casual_leave').bind("change keyup input",function() {
            var cl = $('#input_casual_leave').val();
            $('.casual_leave').val(cl);
        });

        $('input#input_sick_leave').bind("change keyup input",function() {
            var sl = $('#input_sick_leave').val();
            $('.sick_leave').val(sl);
        });

    </script>
@endsection
