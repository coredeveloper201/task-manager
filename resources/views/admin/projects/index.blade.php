@extends('admin.master')

@section('title')
    Mediusware | Projects
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Projects</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/home') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Projects</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <!-- End Bread crumb and right sidebar toggle -->


    <!-- Container fluid  -->

    <div class="page-content container-fluid">

    @include('admin.include.alert')
        <!-- First Cards Row  -->
        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10">
                                <h4 class="card-title">All Projects</h4>
                            </div>
                            <div class="col-md-2 text-right">
                                <a href="{!! route('project.create') !!}" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add New</a>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table id="row_create_call" class="table table-striped table-hover table-bordered display" style="width:100%">
                                <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th>Project Name</th>
                                    <th>Status</th>
                                    <th width="10%">Action</th>
                                </tr>
                                </thead>
                                <tbody id="tablecontents">
                                    @foreach($projects as $key => $project)
                                    <tr class="row1" data-id="{{ $project->id }}">
                                        <td>{!! ++$key !!}</td>
                                        <td>{!! $project->project_name !!}</td>
                                        <td>
                                            @if($project->status == 'active')
                                                <span class="badge badge-pill badge-success">Active</span>
                                            @else
                                                <span class="badge badge-pill badge-danger">Deactivate</span>
                                            @endif
                                        </td>
                                        <td>
                                            <form action="{{ route('project.destroy', $project->id)}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <a href="{!! url('taskman/project/'.$project->id) !!}/edit" class="btn btn-info btn-circle"><i class="fa fa-edit"></i> </a>
                                                <button type="submit" onclick="return confirm('Are you sure...?')" class="btn btn-danger btn-circle"><i class="fa fa-trash"></i> </button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


@endsection

@section('page_js')
    <script type="text/javascript">
        $(function () {
            $( "#tablecontents" ).sortable({
                items: "tr",
                cursor: 'move',
                opacity: 0.6,
                update: function() {
                    sendOrderToServer();
                }
            });

            function sendOrderToServer() {
                var order = [];

                $('tr.row1').each(function(index,element) {
                    order.push({
                        id: $(this).attr('data-id'),
                        position: index+1,
                    });
                });

                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "{{ url('projectman/project/sortable') }}",
                    data: {
                        order:order,
                        _token: '{{csrf_token()}}'
                    },
                    success: function(response) {
                        if (response.status == "success") {
                            console.log(response);
                        } else {
                            console.log(response);
                        }
                    }
                });

            }
        });

    </script>
@endsection
