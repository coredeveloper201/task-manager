@extends('admin.master')

@section('title')
    Mediusware | Interview Exam list
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Exams</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/home') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('exam.index') !!}">Exams</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Exam list</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- Container fluid  -->
    <div class="page-content container-fluid">
    @include('admin.include.alert')
    <!-- First Cards Row  -->
        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10">
                                <h4 class="card-title">{!! $exam->title !!}</h4>
                            </div>
                            <div class="col-md-2 text-right">

                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-bordered display our-dataTable"
                                   style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone No.</th>
                                    <th class="text-center">Approval</th>
                                    <th>Created At</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($exam->applicants as $key => $applicant)
                                    <tr>
                                        <td>{!! ++$key !!}</td>
                                        <td>{!! $applicant->name !!}</td>
                                        <td>{!! $applicant->email !!}</td>
                                        <td>{!! $applicant->phone !!}</td>
                                        <td class="text-center" >
                                            @if($applicant->approval == 1)
                                                <span style="font-size: 16px;" class="badge badge-pill badge-success">Approved</span>
                                            @elseif($applicant->approval == 2)
                                                <span style="font-size: 16px;" class="badge badge-pill badge-danger">Declined</span>
                                            @else($applicant->approval == 0)
                                                <span style="font-size: 16px;" class="badge badge-pill badge-warning">Pending</span>
                                            @endif
                                        </td>
                                        <td>{!! $applicant->created_at->format('d-m-Y h:m A') !!}</td>
                                        <td class="float-right">
                                            @if($applicant->remark !== null)
                                                <button type="button" class="btn btn-primary btn-circle" onclick="showRemark({{ $applicant->id }})"
                                                        data-toggle="tooltip" data-placement="top" title="Remark">
                                                    <i class="fas fa-comment"></i>
                                                </button>
                                            @else
                                                <button type="button" class="btn btn-primary btn-circle" disabled="disabled"
                                                        data-toggle="tooltip" data-placement="top" title="Remark">
                                                    <i class="fas fa-comment"></i>
                                                </button>
                                            @endif
                                            <a href="{{ route('exam-answer.show',$applicant->id) }}" class="btn btn-info btn-circle" data-toggle="tooltip" data-placement="top" title="Result">
                                                <i class="fas fa-eye"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-remark" tabindex="-1" role="dialog" aria-labelledby="vcenter" aria-hidden="true">
            <div class="modal-dialog  modal-dialog-centered">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Remark</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <p id="remark"></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('page_js')
<script type="text/javascript">
    function showRemark(id){
        // alert('');
        $('#modal-remark').modal();
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': "{{csrf_token()}}"
            }
        });
        $.ajax({
            type: 'GET',
            url: "/taskman/applicant/remark_show/"+id,
            dataType: 'JSON',
            success: function (results) {

                if (results.status === 'success') {
                    $("#remark").html(results.data.remark);
                    $('#update_form');
                } else {
                    swal("Error!", results.message, "error");
                }
            }
        });
    }

    $('.our-dataTable').DataTable();

</script>
@endsection
