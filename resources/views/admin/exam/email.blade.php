@extends('layouts.mail')
@section('mail-content')
    <style>
        .card{
            text-align: justify;
            padding: 15px;
        }
        .card-header{
            background: #1f9cd7;
            color: white;
            padding: 15px;
            text-align: center;
            font-size: 20px;
        }
        .card-body{
            background: white;
            padding: 15px;
            margin-bottom: 10px;
        }
        #info p{
            margin: 0px;
        }
    </style>
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <div style="">{{ $applicant->exam->title }}</div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <!-- /.col -->
                    <div class="col-md-10">
                        <div class="box box-primary">
                            <!-- /.box-header -->
                            <div class="box-body no-padding">
                                <!-- /.mailbox-controls -->
                                <div class="mailbox-read-message">
                                    <p>Hello Md. Shahinur Rahman,</p>

                                    <p>Management has decided to ask <b>{{ $applicant->name }}</b> for the interview. Applicant detail information are below.</p>

                                    <hr>
                                        <div class="row" id="info">
                                            <div class="form-group row">
                                                <P><b>Name: </b>{{ $applicant->name }}</P>
                                            </div>
                                            <div class="form-group row">
                                                <p><b>Email: </b>{{ $applicant->email }}</p>
                                            </div>
                                            <div class="form-group row">
                                                <p><b>Phone: </b>{{ $applicant->phone }}</p>
                                            </div>
                                            <div class="form-group row">
                                                <p><b>Address: </b>{{ $applicant->address }}</p>
                                            </div>
                                            <div class="form-group row">
                                                <p><b>Remarks: </b>{{ $applicant->remark }}</p>
                                            </div>
                                        </div>

                                    <p>Thanks,<br>Management</p>
                                </div>
                                <!-- /.mailbox-read-message -->
                            </div>
                            <!-- /.box-footer -->
                        </div>
                        <!-- /. box -->
                    </div>

                    <div class="col-md-1">
                    </div>
                    <!-- /.col -->
                </div>
            </div>
        </div>
    </div>
@endsection
