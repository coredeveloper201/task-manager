@extends('admin.master')

@section('title')
    Mediusware | User Create
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">User Create</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/home') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('user.index') !!}">Users</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Create</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- End Bread crumb and right sidebar toggle -->
    
    
    <!-- Container fluid  -->

    <div class="page-content container-fluid">

        @include('admin.include.alert')

        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10 col-sm-8">
                                <h4 class="card-title">User Create</h4>
                            </div>
                            <div class="col-md-2 col-sm-4 text-right">
                                <a href="{!! route('user.index') !!}" class="btn btn-success"><i class="fa fa-arrow-left"></i> Back</a>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>

                        <form class="" method="post" action="{!! url('taskman/application') !!}" novalidate enctype="multipart/form-data">
                            @csrf
                            <div class="row">

                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <h5>Date From<span class="text-danger">*</span></h5>
                                                <div class="input-group">
                                                    <input type="text" class="form-control{{ $errors->has('date_from') ? ' is-invalid' : '' }} datepicker-autoclose" value="{!! old('date_from', date('Y-m-d')) !!}" required data-validation-required-message="This field is required" name="date_from"  placeholder="Date">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"><i class="icon-calender"></i></span>
                                                    </div>
                                                    @if ($errors->has('date_from'))
                                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('date_from') }}</strong>
                                                </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <h5>Date To<span class="text-danger">*</span></h5>
                                                <div class="input-group">
                                                    <input type="text" class="form-control{{ $errors->has('date_to') ? ' is-invalid' : '' }} datepicker-autoclose" value="{!! old('date_to', date('Y-m-d')) !!}" required data-validation-required-message="This field is required" name="date_to"  placeholder="Date">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"><i class="icon-calender"></i></span>
                                                    </div>
                                                    @if ($errors->has('date_to'))
                                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('date_to') }}</strong>
                                                </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="form-group">
                                        <h5>Subject<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="subject" value="{!! old('subject') !!}" class="form-control{{ $errors->has('subject') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                            @if ($errors->has('subject'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('subject') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="form-group">
                                        <h5>Select Template</h5>
                                        <div class="controls">
                                            <select class="form-control" id="template">
                                                <option>-- Select Template --</option>
                                                @foreach($templates as $template)
                                                    <option value="{!! $template->id !!}">{!! $template->name !!}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="form-group">
                                        <h5>Application<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <textarea id="leTamp" name="application" class="form-control{{ $errors->has('application') ? ' is-invalid' : '' }} summernote" required data-validation-required-message="This field is required">{!! old('application') !!}</textarea>
                                        @if ($errors->has('application'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('application') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="form-group">
                                        <h5>Application File</h5>
                                        <div class="controls">
                                            <input type="file" name="application_file" class="form-control{{ $errors->has('application_file') ? ' is-invalid' : '' }}" accept="image/*, application/pdf">
                                            @if ($errors->has('application_file'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('application_file') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-7">
                                    <div class="form-group validate">
                                        <h5>Leave Type <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <fieldset>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" name="leave_type" value="casual_leave" required="" id="casual_leave" class="custom-control-input" aria-invalid="false">
                                                    <label class="custom-control-label" for="casual_leave">Casual Leave</label>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio"  name="leave_type" value="sick_leave" id="sick_leave" class="custom-control-input" aria-invalid="false">
                                                    <label class="custom-control-label" for="sick_leave">Sick Leave</label>
                                                </div>
                                            </fieldset>
                                            @if ($errors->has('leave_type'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('leave_type') }}</strong>
                                                </span>
                                            @endif
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                            </div>

                            <div class="text-xs-right">
                                <button type="submit" class="btn btn-info">Submit</button>
                                <button type="reset" class="btn btn-inverse">Reset</button>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
        
        <!-- First Cards Row  -->
    </div>


@endsection

@section('page_js')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#template').on('change', function(e) {
            var  tamId = this.value;
            e.preventDefault();
            $.ajax({
                type: "get",
                url: "{!! url('taskman/leave-template/') !!}/" + tamId,
                success: function (data) {
                    console.log(data.obj.template)
                    $('.note-editable p').html(data.obj.template);
                }
            });
        });
    </script>
@endsection
