@extends('admin.master')

@section('title')
    Mediusware | Applications
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Applications</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/home') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Applications</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    
    <!-- End Bread crumb and right sidebar toggle -->
    
    
    <!-- Container fluid  -->

    <div class="page-content container-fluid">

    @include('admin.include.alert')
        <!-- First Cards Row  -->
        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10">
                                <h4 class="card-title">All Applications</h4>
                            </div>
                            <div class="col-md-2 text-right">
                                <a href="{!! route('application.create') !!}" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add New</a>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table id="row_create_call" class="table table-striped table-hover table-bordered display" style="width:100%">
                                <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th>Subject</th>
                                    <th>Leave Type</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th width="10%">Action</th>
                                </tr>
                                </thead>
                                <tbody id="tablecontents">
                                    @foreach($applications as $key => $application)
                                    <tr class="row1" data-id="{{ $application->id }}">
                                        <td>{!! ++$key !!}</td>

                                        <td>{!! $application->subject !!}</td>
                                        <td>
                                            @if($application->leave_type == 'casual_leave')
                                                Casual Leave
                                            @else
                                                Sick Leave
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            {!! date('F d, Y', strtotime($application->date_from)).' - '. date('F d, Y', strtotime($application->date_to)) !!}<br>
                                            ({!! $application->date_count !!} Day)
                                        </td>

                                        <td>
                                            @if($application->status == 'pending')
                                                <span style="font-size: 18px;" class="badge badge-pill badge-warning">Pending</span>
                                            @elseif($application->status == 'approved')
                                                <span style="font-size: 18px;" class="badge badge-pill badge-success">Approved</span>
                                            @else
                                                <span style="font-size: 18px;" class="badge badge-pill badge-danger">Rejected</span>
                                            @endif
                                        </td>
                                        <td>
                                            <form action="{{ route('application.destroy', $application->id)}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <a href="javascript:void(0)" data-id="{{ $application->id }}" data-toggle="modal" data-target="#portfolioModal" class="btn btn-success btn-circle viewPortfolio"><i class="fa fa-eye"></i> </a>
                                                @if($application->status == 'pending')
                                                    <a href="{!! url('taskman/application/'.$application->id) !!}/edit" class="btn btn-info btn-circle"><i class="fa fa-edit"></i> </a>
                                                    <button type="submit" onclick="return confirm('Are you sure...?')" class="btn btn-danger btn-circle"><i class="fa fa-trash"></i> </button>
                                                @endif
                                            </form>

                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="modal fade bs-example-modal-lg" id="portfolioModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none; z-index: 1041;">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Application</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="modal-body">
                        <p><b id="date"></b></p>
                        <p style="margin-top: -15px;">Subject: <b id="subject"></b></p>
                        <p id="body"></p>
                        <p class="text-capitalize">Leave Type: <b id="leaveType"></b></p>
                        <p style="margin-top: -15px;" class="text-capitalize">Status: <b id="status"></b></p>

                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered text-center">
                            <tr>
                                <th>#</th>
                                <th>Casual Leave</th>
                                <th>Sick Leave</th>
                            </tr>

                            <tr>
                                <th>Leaves</th>
                                <td id="casualLeave">Casual Leave</td>
                                <td id="sickLeave">Sick Leave</td>
                            </tr>

                            <tr>
                                <th>Get Leave</th>
                                <td id="tCasual">Casual Leave</td>
                                <td id="tSick">Sick Leave</td>
                            </tr>

                            <tr>
                                <th>Remaining Leave</th>
                                <td id="reCasual">Casual Leave</td>
                                <td id="reSick">Sick Leave</td>
                            </tr>

                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <a id="appFilePdf" target="_blank" style="display: none" class="btn btn-rounded btn-outline-success mr-2 btn-sm"><i class="fa fa-file"></i> Show File</a>
                    <a id="appFileImg" style="display: none" class="btn btn-rounded btn-outline-success mr-2 btn-sm image-popup-vertical-fit"><i class="fa fa-file"></i> Show File</a>
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection

@section('page_js')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(".viewPortfolio").click(function (e) {
            var id = $(this).attr("data-id");
            e.preventDefault();
            $.ajax({
                type: "get",
                url: "{!! url('taskman/application/') !!}/" + id,
                success: function (data) {
                    $("#date").html(data.date);
                    $("#subject").html(data.obj.subject);
                    $("#body").html(data.obj.application);
                    $("#leaveType").html(data.leave_type);
                    $("#status").html(data.obj.status);
                    $("#tCasual").html(data.obj.t_casual);
                    $("#tSick").html(data.obj.t_sick);
                    $("#casualLeave").html(data.leave.casual_leave);
                    $("#sickLeave").html(data.leave.sick_leave);
                    $("#reCasual").html(data.leave.casual_leave - data.obj.t_casual);
                    $("#reSick").html(data.leave.sick_leave - data.obj.t_sick);

                    if (data.check_file == ''){
                        $("#appFilePdf").hide();
                        $("#appFileImg").hide()
                    } else {
                        if (data.check_file == 'pdf') {
                            $("#appFileImg").hide();
                            $("#appFilePdf").show();
                            $("a#appFilePdf").attr("href", data.file_path);
                        }else {
                            $("#appFilePdf").hide();
                            $("#appFileImg").show();
                            $("a#appFileImg").attr("href", data.file_path);
                        }
                    }

                }
            });
        });

    </script>
@endsection
