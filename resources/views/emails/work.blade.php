@extends('layouts.mail')

@section('mail-content')
    <div style="padding: 40px; background: #fff;">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tbody>
                <tr>
                    <td><b>Hello {!! $supervisor_name !!}....!</b>
                        <p>Task complete from MediusWare.Com</p>
                        <span><b>Work Details: </b>{!! $work_details !!}</span>
                        <span><b>Work Complete Date: </b>{!! $complete_date !!}</span>
                        <hr>
                        <span>Task Details:</span><br>
                        <span><b>Employee name: </b>{!! $employee_name !!}</span><br>
                        <span><b>Task Name: </b>{!! $task_name !!}</span><br>
                        <span><b>Task Date:
                            </b>{!! date('F d, Y', strtotime($start_date)).' - '. date('F d, Y', strtotime($end_date)) !!}
                        </span><br>
                        <span><b>Task Duration: </b>{!! $ratio !!} Days</span>
                </tr>
            </tbody>
        </table>
    </div>
@endsection
