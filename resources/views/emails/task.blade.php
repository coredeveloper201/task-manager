@extends('layouts.mail')

@section('mail-content')
    <div style="padding: 40px; background: #fff;">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tbody>
                <tr>
                    <td><b>Hello {!! $employee_name !!}....!</b>
                        <p>You have a new task from MediusWare.Com</p>
                        <span>Supervisor: <b>{!! $supervisor_name !!}</b></span><br>
                        <span><b>Project Name: </b>{!! $project_name !!}</span><br>
                        <span><b>Task Name: </b>{!! $task_name !!}</span><br>
                        <span><b>Task Date: </b>{!! date('F d, Y', strtotime($start_date)).' - '. date('F d, Y', strtotime($end_date)) !!}</span><br>
                        <span><b>Task Duration: </b>{!! $ratio !!} Days</span>
                </tr>
            </tbody>
        </table>
    </div>
@endsection
