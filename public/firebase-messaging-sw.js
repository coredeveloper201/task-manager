// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js');


var config = {
    apiKey: "AIzaSyCI887TRD-x8JetVdavH_AW1hmwbrYxaFs",
    authDomain: "laravel-test-6d8a7.firebaseapp.com",
    databaseURL: "https://laravel-test-6d8a7.firebaseio.com",
    projectId: "laravel-test-6d8a7",
    storageBucket: "laravel-test-6d8a7.appspot.com",
    messagingSenderId: "816860377990"
};
firebase.initializeApp(config);


// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function (payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    /*return self.registration.showNotification(notificationTitle, notificationOptions);*/
});
