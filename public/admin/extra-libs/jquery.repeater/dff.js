var room = 1;

function education_fields() {

    room++;
    var objTo = document.getElementById('education_fields')
    var divtest = document.createElement("div");
    divtest.setAttribute("class", "form-group removeclass" + room);
    var rdiv = 'removeclass' + room;
    divtest.innerHTML =
        '<div class="row">'+
            '<div class="col-md-5">'+
                '<div class="form-group">'+
                    '<div class="controls">'+
                        '<input type="text" name="skills_name[]" value="" placeholder="Skills Name: HTML" class="form-control" required data-validation-required-message="This field is required">'+

                    '</div>'+
                '</div>'+
            '</div>'+

            '<div class="col-md-5">'+
                '<div class="form-group">'+
                    '<div class="controls">'+
                        '<input type="number" min="10" name="skills_percentage[]" value=""  placeholder="Skills Percentage: 85" class="form-control" required data-validation-required-message="This field is required">'+
                    '</div>'+
                '</div>'+
            '</div>'+

            '<div class="col-sm-2"> ' +
                '<div class="form-group"> ' +
                    '<button class="btn btn-danger" type="button" onclick="remove_education_fields(' + room + ');"> <i class="fa fa-minus"></i> </button> ' +
                '</div>' +
            '</div>'+
        '</div>';

    objTo.appendChild(divtest)
}

function remove_education_fields(rid) {
    $('.removeclass' + rid).remove();
}