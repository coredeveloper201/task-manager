<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $guarded = ['id'];

    public function exam()
    {
        return $this->belongsTo(Exam::class, 'question_id', 'id');
    }

    public function applicantAnswers()
    {
        return $this->hasMany(ApplicantAnswer::class);
    }

    public function applicantAnswer()
    {
        return $this->hasOne(ApplicantAnswer::class, 'question_id', 'id');
    }
}
