<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApplicantAnswer extends Model
{
    protected $guarded = ['id'];

    public function applicant()
    {
        return $this->belongsTo(Applicant::class);
    }

    public function question()
    {
        return $this->hasMany(Question::class);
    }
}
