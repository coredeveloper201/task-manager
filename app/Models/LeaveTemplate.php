<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LeaveTemplate extends Model
{
    protected $table = 'leave_templates';
}
