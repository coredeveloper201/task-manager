<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Task extends Model
{
    protected $table = 'tasks';

    public function employees(){
        return $this->hasMany('App\Models\TaskEmployeeRelation', 'task_id', 'id');
    }

    public function supervisor(){
        return $this->belongsTo('App\User', 'super_vision', 'id');
    }

    public function images(){
        return $this->hasMany('App\Models\MediaImage', 'task_id', 'id');
    }

    public function taskLists(){
        return $this->hasMany('App\Models\TaskList', 'task_id', 'id');
    }

    public function project(){
        return $this->belongsTo('App\Models\Project', 'project_id', 'id');
    }

    public function works(){
        return $this->hasMany('App\Models\Work', 'task_id', 'id');
    }

    public function checkWorks(){
        return $this->hasMany('App\Models\Work', 'task_id', 'id')->where('employee_id', Auth::user()->id);
    }
}
