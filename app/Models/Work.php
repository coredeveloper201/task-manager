<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
    protected $table = 'works';


    public function task(){
        return $this->belongsTo('App\Models\Task', 'task_id', 'id');
    }

    public function images(){
        return $this->hasMany('App\Models\MediaImage', 'work_id', 'id');
    }

    public function reviews(){
        return $this->hasMany('App\Models\Review', 'work_id', 'id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'employee_id', 'id');
    }


}
