<?php

namespace App\Http\Controllers;

use App\Models\About;
use App\Models\Category;
use App\Models\Client;
use App\Models\Contact;
use App\Models\Feature;
use App\Models\InformativeText;
use App\Models\NewsletterSubscription;
use App\Models\Portfolio;
use App\Models\Service;
use App\Models\Setting;
use App\Models\Slider;
use App\Models\Team;
use App\Models\Technology;
use App\Models\Testimonial;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Mail;
use Validator;

class WebsiteController extends Controller
{
    public function index(){
        return view('auth.login');
    }

    public function home(){
        return view('website.home');
    }
}
