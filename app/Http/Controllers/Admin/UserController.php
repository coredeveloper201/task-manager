<?php

namespace App\Http\Controllers\Admin;

use App\Models\Task;
use App\Models\TaskEmployeeRelation;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Validator;
use Image;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('role', '!=', 'admin')->orderBy('sort', 'ASC')->get();
        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'role' => Rule::in(['management', 'employee']),
            'name' => 'required|max:191',
            'salary' => 'required',
            'email' => 'required|email|unique:users|max:191',
            'password' => 'required|string|min:6|confirmed',
            'status' => Rule::in(['active', 'deactivate']),
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $user  = new User();

        if($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = $image->getClientOriginalExtension();
            $fileName = time() . "." . $imageName;
            $directory = 'media/user/';
            $imageUrl = $directory.$fileName;
            Image::make($image)->resize(300, 300)->save($imageUrl);
            $user->image = $fileName;
        }

        $user->role      = $request->role;
        $user->name      = $request->name;
        $user->email     = $request->email;
        $user->salary    = $request->salary;
        $user->joining_date    = $request->joining_date;
        $user->permanent_date    = $request->permanent_date;
        $user->phone     = $request->phone;
        $user->password  = Hash::make($request->password);
        $user->status    = $request->status;
        $user->save();
        return redirect(route('user.index'))->with('success', 'User save successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $taskQueries = Task::orderBy('id', 'DESC');
        if ($id){
            $taskQueries->join('task_employee_relations as re_task', 'tasks.id', '=', 're_task.task_id');
            $taskQueries->join('users', 'users.id', '=', 're_task.employee_id');
            $taskQueries->select('tasks.*');
            $taskQueries->where('re_task.employee_id', '=', $id);
        }
        $tasks = $taskQueries->paginate(2);
        $user = User::find($id);
        return view('admin.users.view', compact('tasks', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'role' => Rule::in(['management', 'employee']),
            'name' => 'required|max:191',
            'status' => Rule::in(['active', 'deactivate']),
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $user = User::find($id);

        if($request->hasFile('image')) {
            if ($user->image){
                unlink('media/user/'. $user->image);
            }
            $image = $request->file('image');
            $imageName = $image->getClientOriginalExtension();
            $fileName = time() . "." . $imageName;
            $directory = 'media/user/';
            $imageUrl = $directory.$fileName;
            Image::make($image)->resize(300, 300)->save($imageUrl);
            $user->image = $fileName;
        }

        $user->role    = isset($request->role) ? $request->role:$user->role;
        $user->name    = $request->name;
        $user->about   = $request->about;
        $user->salary  = isset($request->salary) ? $request->salary:$user->salary;
        $user->joining_date    = $request->joining_date;
        $user->permanent_date    = $request->permanent_date;
        $user->phone   = $request->phone;
        $user->status  = isset($request->status) ? $request->status:$user->status;
        $user->save();
        return redirect()->back()->with('success', 'User update successfully');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'password' => 'required|max:191|min:6',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $user = User::find($id);

        $user->password    = Hash::make($request->password);
        $user->save();
        return redirect()->back()->with('success', 'User update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if ($user->image){
            unlink('media/user/'. $user->image);
        }
        $user->delete();
        return redirect()->back()->with('error', 'User deleted!!');
    }

    public function sortable(Request $request){
        $users = User::all();
        foreach ($users as $user) {
            $user->timestamps = false; // To disable update_at field updation
            $id = $user->id;
            foreach ($request->order as $order) {
                if ($order['id'] == $id) {
                    $user->update(['sort' => $order['position']]);
                }
            }
        }

        return response('Update Successfully.', 200);
    }

    public function resetPassword(Request $request, $id){
        $user = User::find($id);
        $oldPassword = $request->old_password;
        if (Hash::check($oldPassword, $user->password)) {
            $data = $request->all();
            $validator = Validator::make($data, [
                'password' => 'required|string|min:6|confirmed',
            ]);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }

            $user->password  = Hash::make($request->password);
            $user->save();
            return redirect()->back()->with('success', 'Your password change successfully!');
        }else {
            return redirect()->back()->with('error', 'Your old password not match!');
        }

    }

    public function resetEmail(Request $request, $id){

        $data = $request->all();
        $validator = Validator::make($data, [
            'email' => 'required|email|unique:users|max:191',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = User::find($id);
        $user->email = $request->email;
        $user->save();
        return redirect()->back()->with('success', 'Your password change successfully!');
    }
}
