<?php

namespace App\Http\Controllers\Admin;

use App\Models\Leave;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class LeaveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $leaves = DB::table('leaves')
            ->select('year', DB::raw('count(user_id) as total_user'))
            ->groupBy('year')
            ->get();
        return view('admin.leave.index', compact('leaves'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::where('role', '!=', 'admin')->where('status', 'active')->get();
        return view('admin.leave.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        if (!empty($data['casual_leave'] && $data['sick_leave'])){
            $leaveDate = [];
            foreach ($data['user_id'] as $k => $userId) {
                $checkLeave = Leave::where('year', $data['year'])->where('user_id', $userId)->first();
                if (empty($checkLeave)) {
                    $leaveDate[] = [
                        'user_id'      => $userId,
                        'year'         => $data['year'],
                        'casual_leave' => $data['casual_leave'][$k],
                        'sick_leave'   => $data['sick_leave'][$k],
                    ];
                }

            }
            if (!empty($leaveDate)) {
                Leave::insert($leaveDate);
            }
        }
        return redirect()->back()->with('success', 'Leave save successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($year)
    {
        $leaves = Leave::with('user')->where('year', $year)->whereHas('user', function($q){
            $q->where('status', 'active');
        })->get();
        return view('admin.leave.edit', compact('leaves'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function leaveUpdate(Request $request)
    {
        for($i = 0; $i< count($request->id); $i++){
            $leave = Leave::findOrFail($request->id[$i]);
            $leave->casual_leave = $request->casual_leave[$i];
            $leave->sick_leave   = $request->sick_leave[$i];
            $leave->save();
        }
        return redirect()->back()->with('success', 'Leave update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function yearlyLeaveReport(Request $request){
        if (isset($request->year)){
            $year = $request->year;
        }else{
            $year = date('Y');
        }

        $leaves = Leave::select('leaves.casual_leave', 'leaves.sick_leave','leaves.user_id', \DB::raw('IFNULL(B.t_casual, 0) AS t_casual'), \DB::raw('IFNULL(C.t_sick, 0) AS t_sick'))
            ->leftJoin(\DB::raw("(SELECT SUM(date_count) as t_casual, user_id FROM leave_applications WHERE YEAR(date_from) >= $year AND YEAR(date_from) <= $year AND leave_type='casual_leave' AND status='approved' GROUP BY user_id) AS B"), 'leaves.user_id', '=', 'B.user_id')
            ->leftJoin(\DB::raw("(SELECT SUM(date_count) as t_sick, user_id FROM leave_applications WHERE YEAR(date_from) >= $year AND YEAR(date_from) <= $year AND leave_type='sick_leave' AND status='approved' GROUP BY user_id) AS C"), 'leaves.user_id', '=', 'C.user_id')
            ->where('leaves.year', $year)
            ->with('user')->whereHas('user', function($q){
                $q->where('status', 'active');
            })->get();
//        return $leaves;
        return view('admin.leave.yearly_leave_report', compact('leaves', 'year'));
    }

}
