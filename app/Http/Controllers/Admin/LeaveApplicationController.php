<?php

namespace App\Http\Controllers\Admin;

use App\Models\FcmToken;
use App\Models\Leave;
use App\Models\LeaveApplication;
use App\Models\LeaveTemplate;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;
use PharIo\Manifest\Application;
use Validator;
use Auth;
use DB;

class LeaveApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $applications = LeaveApplication::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->get();
        return view('admin.application.index',compact('applications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $templates = LeaveTemplate::where('status', 'active')->get();
        return view('admin.application.create', compact('templates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'date_from' => 'required|date',
            'date_to' => 'required|date',
            'subject' => 'required|max:191',
            'application' => 'required',
            'leave_type' => Rule::in(['casual_leave', 'sick_leave']),
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $startTimeStamp = strtotime($data['date_from']);
        $endTimeStamp   = strtotime($data['date_to']);
        $timeDiff       = abs($endTimeStamp - $startTimeStamp);
        $numberDays     = $timeDiff/86400;  // 86400 seconds in one day
        $numberDays     = 1 + intval($numberDays);

        $application              = new LeaveApplication();
        $application->user_id     = Auth::user()->id;
        $application->date_from   = $data['date_from'];
        $application->date_to     = $data['date_to'];
        $application->subject     = $data['subject'];
        $application->application = $data['application'];
        $application->leave_type  = $data['leave_type'];
        $application->date_count  = $numberDays;
        if ($request->hasFile('application_file')) {
            $file = $request->file('application_file');
            $name = Auth::user()->name.'_'.time().'.'. $file->getClientOriginalExtension();
            $file->move('media/application-file/', $name);
            $application->application_file = $name;
        }
        if ($application->save()){
            $adminOrMmns = User::where('role', '!=', 'employee')->get();
            foreach ($adminOrMmns as $adminOrMmn){
                $email = $adminOrMmn->email;
                Mail::send('emails.leave_application', [
                    'employee_name'   => Auth::user()->name,
                    'subject'         => $request->subject,
                    'start_date'      => date('F d, Y', strtotime($request->date_from)),
                    'date_to'         => date('F d, Y', strtotime($request->date_to)),
                    'body'            => $request->application,
                    'leave_type'      => $request->leave_type,
                    'numberDays'      => $numberDays,
                ], function ($msg) use ($email, $request)
                {
                    $msg->from('info@mediusware.com', 'MediusWare.Com');
                    $msg->subject("Leave Application | ".$request->subject);
                    $msg->to($email);
                });
            }
        }
        return redirect(route('application.index'))->with('success', 'Your application is pending, wait for management approval');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $year = date('Y', strtotime(LeaveApplication::find($id)->created_at));
        $application = LeaveApplication::select('leave_applications.*', \DB::raw('IFNULL(B.t_casual, 0) AS t_casual'), \DB::raw('IFNULL(C.t_sick, 0) AS t_sick'))
            ->leftJoin(\DB::raw("(SELECT SUM(date_count) as t_casual, user_id FROM leave_applications WHERE YEAR(date_from) >= $year AND YEAR(date_from) <= $year AND leave_type='casual_leave' AND status='approved' GROUP BY user_id) AS B"), 'leave_applications.user_id', '=', 'B.user_id')
            ->leftJoin(\DB::raw("(SELECT SUM(date_count) as t_sick, user_id FROM leave_applications WHERE YEAR(date_from) >= $year AND YEAR(date_from) <= $year AND leave_type='sick_leave' AND status='approved' GROUP BY user_id) AS C"), 'leave_applications.user_id', '=', 'C.user_id')
            ->find($id);

        $leave = Leave::where('user_id', $application->user_id)
            ->where('year', date('Y', strtotime($application->date_from)))
            ->first();

        $fileExtCheck = \File::extension($application->application_file);
        if ($application->date_from == $application->date_to){
            $dayNdate= date('F d, Y', strtotime($application->date_from))." ($application->date_count Day)";
        }else{
            $dayNdate = date('F d, Y', strtotime($application->date_from))." - ".date('F d, Y', strtotime($application->date_to))." ($application->date_count Days)";
        }

        return response()->json([
            'message' => 'success',
            'obj' => $application,
            'leave' => $leave,
            'date' => $dayNdate,
            'check_file' => $fileExtCheck,
            'file_path' => asset('media/application-file/'.$application->application_file),
            'leave_type' => $application->leave_type == 'casual_leave' ? 'Casual Leave':'Sick Leave',
            'change_leave' => $application->leave_type,
            'approveUrl' => url('taskman/application/approve/'.$application->id),
            'rejectUrl' => url('taskman/application/reject/'.$application->id),
            'casualToSick' => url('taskman/casualToSick/'.$application->id),
            'sickToCasual' => url('taskman/sickToCasual/'.$application->id),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $application = LeaveApplication::find($id);
        $fileExt = \File::extension($application->application_file);
        return view('admin.application.edit', compact('application', 'fileExt'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'date_from'   => 'required|date',
            'date_to'     => 'required|date',
            'subject'     => 'required|max:191',
            'application' => 'required',
            'leave_type'  => Rule::in(['casual_leave', 'sick_leave']),
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $startTimeStamp = strtotime($data['date_from']);
        $endTimeStamp   = strtotime($data['date_to']);
        $timeDiff       = abs($endTimeStamp - $startTimeStamp);
        $numberDays     = $timeDiff/86400;  // 86400 seconds in one day
        $numberDays     = 1 + intval($numberDays);

        $application              = LeaveApplication::find($id);
        $application->date_from   = $data['date_from'];
        $application->date_to     = $data['date_to'];
        $application->subject     = $data['subject'];
        $application->application = $data['application'];
        $application->leave_type  = $data['leave_type'];
        $application->date_count  = $numberDays;
        if ($request->hasFile('application_file')) {
            if ($application->application_file){
                if (!file_exists(public_path('media/application-file/'.$application->application_file))){
                    $application->application_file = null;
                }else{
                    unlink('media/application-file/'. $application->application_file);
                }
            }
            $file = $request->file('application_file');
            $name = Auth::user()->name.'_'.time().'.'. $file->getClientOriginalExtension();
            $file->move('media/application-file/', $name);
            $application->application_file = $name;
        }

        $application->save();
        return redirect(route('application.index'))->with('success', 'Your application update successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $application = LeaveApplication::find($id);
        if ($application->application_file){
            if (!file_exists(public_path('media/application-file/'.$application->application_file))){
                $application->application_file = null;
            }else{
                unlink('media/application-file/'. $application->application_file);
            }
        }
        $application->delete();
        return redirect(route('application.index'))->with('success', 'Your application delete successfully!');
    }


    public function empApplication(){
        $applications = LeaveApplication::orderBy('id', 'desc')->with('user')->whereHas('user', function($q){
            $q->where('status', 'active');
        })->get();
        return view('admin.application.emp_applications', compact('applications'));
    }

    public function applicationApprove($id){
        $application = LeaveApplication::find($id);
        $application->status = 'approved';
        $application->isApproved = 1;
        if($application->save()){
            $userFcmTokens = FcmToken::where('user_id', $application->user_id)->get();
            foreach ($userFcmTokens as $fcmToken):
                $post = array(
                    "to" => $fcmToken->fcm_token,
                    "collapse_key" => "green",
                    "notification" => [
                        "title" => "Leave Notification",
                        "body" => "Your Leave approved",
                        "icon" => asset('/admin/images/logos/logo-icon.png'),
                        "image" => "",
                    ]
                );
                fcmFire($post);
            endforeach;

            $employee = User::find($application->user_id);
            $email = $employee->email;
            Mail::send('emails.leave_application_feedback', [
                'employee_name'   => $employee->name,
                'subject'         => $application->subject,
                'start_date'      => date('F d, Y', strtotime($application->date_from)),
                'date_to'         => date('F d, Y', strtotime($application->date_to)),
                'body'            => $application->application,
                'leave_type'      => $application->leave_type,
                'numberDays'      => $application->date_count,
                'status'          => "Your Application Approved",
            ], function ($msg) use ($email)
            {
                $msg->from('info@mediusware.com', 'MediusWare.Com');
                $msg->subject("Leave Application Approved");
                $msg->to($email);
            });

        }
        return redirect()->back()->with('success', 'Leave approved successfully!');
    }

    public function applicationReject($id){
        $application = LeaveApplication::find($id);
        $application->status = 'rejected';
        if($application->save()){
            $userFcmTokens = FcmToken::where('user_id', $application->user_id)->get();
            foreach ($userFcmTokens as $fcmToken):
                $post = array(
                    "to" => $fcmToken->fcm_token,
                    "collapse_key" => "green",
                    "notification" => [
                        "title" => "Leave Notification",
                        "body" => "Your Leave rejected",
                        "icon" => asset('/admin/images/logos/logo-icon.png'),
                        "image" => "",
                    ]
                );
                fcmFire($post);
            endforeach;

            $employee = User::find($application->user_id);
            $email = $employee->email;
            Mail::send('emails.leave_application_feedback', [
                'employee_name'   => $employee->name,
                'subject'         => $application->subject,
                'start_date'      => date('F d, Y', strtotime($application->date_from)),
                'date_to'         => date('F d, Y', strtotime($application->date_to)),
                'body'            => $application->application,
                'leave_type'      => $application->leave_type,
                'numberDays'      => $application->date_count,
                'status'          => "Your Application Rejected",
            ], function ($msg) use ($email)
            {
                $msg->from('info@mediusware.com', 'MediusWare.Com');
                $msg->subject("Leave Application Rejected");
                $msg->to($email);
            });
        }
        return redirect()->back()->with('success', 'Leave approved successfully!');
    }


    // Change casual to sick
    public function casualToSick($id){
        $application = LeaveApplication::find($id);
        $application->leave_type = 'sick_leave';
        $application->changeId = Auth::user()->id;
        if($application->save()){
            $userFcmTokens = FcmToken::where('user_id', $application->user_id)->get();
            foreach ($userFcmTokens as $fcmToken):
                $post = array(
                    "to" => $fcmToken->fcm_token,
                    "collapse_key" => "green",
                    "notification" => [
                        "title" => "Leave Change Notification",
                        "body" => Auth::user()->role. " change your causal leave to sick leave",
                        "icon" => asset('/admin/images/logos/logo-icon.png'),
                        "image" => "",
                    ]
                );
                fcmFire($post);
            endforeach;

            $employee = User::find($application->user_id);
            $email = $employee->email;
            Mail::send('emails.leave_application_change', [
                'author_role'     => Auth::user()->role,
                'employee_name'   => $employee->name,
                'subject'         => $application->subject,
                'start_date'      => date('F d, Y', strtotime($application->date_from)),
                'date_to'         => date('F d, Y', strtotime($application->date_to)),
                'body'            => $application->application,
                'leave_type'      => $application->leave_type,
                'numberDays'      => $application->date_count,
                'status'          => "change your causal leave to sick leave",
            ], function ($msg) use ($email)
            {
                $msg->from('info@mediusware.com', 'MediusWare.Com');
                $msg->subject("Leave application change casual to sick");
                $msg->to($email);
            });
        }
        return redirect()->back()->with('success', 'Leave change causal to sick successfully!');
    }

    // Change sick to casual
    public function sickToCasual($id){
        $application = LeaveApplication::find($id);
        $application->leave_type = 'casual_leave';
        $application->changeId = Auth::user()->id;
        if($application->save()){
            $userFcmTokens = FcmToken::where('user_id', $application->user_id)->get();
            foreach ($userFcmTokens as $fcmToken):
                $post = array(
                    "to" => $fcmToken->fcm_token,
                    "collapse_key" => "green",
                    "notification" => [
                        "title" => "Leave Change Notification",
                        "body" => Auth::user()->role. " change your sick leave to causal leave",
                        "icon" => asset('/admin/images/logos/logo-icon.png'),
                        "image" => "",
                    ]
                );
                fcmFire($post);
            endforeach;

            $employee = User::find($application->user_id);
            $email = $employee->email;
            Mail::send('emails.leave_application_change', [
                'author_role'     => Auth::user()->role,
                'employee_name'   => $employee->name,
                'subject'         => $application->subject,
                'start_date'      => date('F d, Y', strtotime($application->date_from)),
                'date_to'         => date('F d, Y', strtotime($application->date_to)),
                'body'            => $application->application,
                'leave_type'      => $application->leave_type,
                'numberDays'      => $application->date_count,
                'status'          => "change your sick leave to causal leave",
            ], function ($msg) use ($email)
            {
                $msg->from('info@mediusware.com', 'MediusWare.Com');
                $msg->subject("Leave application change sick to casual");
                $msg->to($email);
            });
        }
        return redirect()->back()->with('success', 'Leave change sick to causal successfully!');
    }

}
