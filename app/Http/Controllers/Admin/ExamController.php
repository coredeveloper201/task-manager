<?php

namespace App\Http\Controllers\Admin;

use App\Models\Applicant;
use App\Models\Exam;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Symfony\Component\HttpFoundation\Response;
use Validator;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $exams = Exam::with('questions')->get();
        return view('admin.exam.index', compact('exams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'title' => 'required',
        ));
        if ($validator->fails()) {
            return redirect()->route('exam.index');
        }
        $exam = Exam::create([
            'title' => $request->title,
            'slug' => $this->createSlug($request->title),
            'comprehensive' => $request->comprehensive,
        ]);
        if ($exam) {
            return redirect()->route('exam.index');
        } else {
            return redirect()->route('exam.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $exam = Exam::findOrFail($id);
        return view('admin.exam.questions', compact('exam'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        $exam = Exam::find($id);
        if ($exam) {
            return response()->json(['success' => true, 'data' => $exam, 'status' => 200], Response::HTTP_OK);
        } else {
            return response()->json(['success' => false, 'data' => 'Exam Not Found', 'status' => 401], Response::HTTP_OK);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), array(
            'title' => 'required',
        ));
        if ($validator->fails()) {
            return redirect()->route('exam.index');
        }
        $data = array(
            'title' => $request->title,
            'slug' => $this->createSlug($request->title),
            'comprehensive' => $request->comprehensive,
        );
        $exam = Exam::findOrFail($id);
        if ($exam->title == $request->title) {
            unset($data['slug']);
        }
        $exam = $exam->update($data);
        if ($exam) {
            return redirect()->route('exam.index');
        } else {
            return redirect()->route('exam.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $hasApplicant = Applicant::where('exam_id', $id)->first();
        if ($hasApplicant) {
            Session::flash('hasApplicant', 'Whoops! Can not delete! Some people has given exam on this.');
            return redirect()->route('exam.index');
        }
        $exam = Exam::findOrFail($id);
        if ($exam->questions->count() > 0) {
            $exam->questions()->delete();
        }
        $destroy = $exam->delete();
        if ($destroy) {
            Session::flash('deleted', 'Exam deleted Successfully');
            return redirect()->route('exam.index');
        } else {
            Session::flash('deleted', 'Whoops! Exam not deleted');
            return redirect()->route('exam.index');
        }
    }

    private function createSlug($title, $id = 0)
    {
        $slug = str_slug($title);
        $allSlugs = $this->getRelatedSlugs($slug, $id);
        if (!$allSlugs->contains('slug', $slug)) {
            return $slug;
        }

        $i = 1;
        $is_contain = true;
        do {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains('slug', $newSlug)) {
                $is_contain = false;
                return $newSlug;
            }
            $i++;
        } while ($is_contain);
    }

    private function getRelatedSlugs($slug, $id = 0)
    {
        return Exam::select('slug')->where('slug', 'like', $slug . '%')
            ->where('id', '<>', $id)
            ->get();
    }

    /**
     * Change The Activity Status of the exam
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeActivity($id)
    {
        $exam = Exam::find($id);
        $status = 0;
        if ($exam->status == 0) {
            $status = 1;
        }
        $exam = $exam->update(['status' => $status]);
        if ($exam) {
            return response()->json(['success' => true, 'Status updated Successfully', 'status' => 200], 200);
        } else {
            return response()->json(['success' => false, 'Whoops! Status not updated', 'status' => 401], 200);
        }
    }

    /**
     * Show the Question list with the exam ID
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showQuestions($id)
    {
        $exam = Exam::with('questions')->find($id);
        if ($exam) {
            return response()->json(['success' => true, 'data' => $exam, 'status' => 200], 200);
        } else {
            return response()->json(['success' => false, 'Whoops! Exam not found', 'status' => 401], 200);
        }
    }

    /**
     * Show the Examine List from a Exam Id
     *
     * @param int $exam_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showExamine($exam_id)
    {
        $exam = Exam::with(['applicants' => function ($q) {
            $q->orderBy('approval');
        }])->findOrFail($exam_id);
        return view('admin.exam.examinee.index', compact('exam'));
    }
}
