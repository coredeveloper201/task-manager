<?php

namespace App\Http\Controllers\Admin;

use App\Models\MediaImage;
use App\Models\Project;
use App\Models\Review;
use App\Models\Task;
use App\Models\TaskEmployeeRelation;
use App\Models\TaskList;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;
use Validator;
use Auth;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::orderBy('sort', 'ASC')->get();
        return view('admin.tasks.index', compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = User::where('role', 'employee')->orderBy('sort', 'ASC')->get();
        $projects = Project::get();
        return view('admin.tasks.create', compact('employees', 'projects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'employee' => 'required',
            'name' => 'required|max:191',
            'task_details' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'status' => Rule::in(['active', 'deactivate', 'complete']),
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $startTimeStamp = strtotime($request->start_date);
        $endTimeStamp = strtotime($request->end_date);
        $timeDiff = abs($endTimeStamp - $startTimeStamp);
        $numberDays = $timeDiff/86400;  // 86400 seconds in one day
        $numberDays = 1 + intval($numberDays);

        $task = new Task();
        $task->super_vision = Auth::user()->id;
        $task->project_id   = $request->project;
        $task->name         = $request->name;
        $task->task_details = $request->task_details;
        $task->start_date   = $request->start_date;
        $task->end_date     = $request->end_date;
        $task->ratio        = $numberDays;
        $task->note         = $request->note;
        $task->status       = $request->status;
        if ($task->save()){
            if (!empty($data['employee'])) {
                $employeeData = [];
                foreach ($data['employee'] as $k => $employeeId) {
                    $employeeData[] = [
                        'task_id' => $task->id,
                        'employee_id' => $employeeId,
                    ];
                }
                if (!empty($employeeData)) {
                    TaskEmployeeRelation::insert($employeeData);
                }
            }

            if (!empty($data['task_list'])) {
                //array filter for zero empty value check
                $task_lists = $data['task_list'];
                $task_lists = !empty($task_lists) ? array_values(array_filter($task_lists)) : array();
                $listData = [];
                foreach ($task_lists as $k => $task_list) {
                    $listData[] = [
                        'task_id'   => $task->id,
                        'list_name' => $task_list,
                    ];
                }
                if (!empty($listData)) {
                    TaskList::insert($listData);
                }
            }

            if($request->hasfile('images'))
            {
                $images = [];
                foreach($request->file('images') as $key => $image)
                {
                    $name = $image->getClientOriginalName();
                    $mainName = time().'_'. $name;
                    $image->move(public_path().'/media/task/',$mainName);
                    $images[] = [
                        'task_id' => $task->id,
                        'image' => $mainName,
                    ];
                }

                if (!empty($images)) {
                    MediaImage::insert($images);
                }
            }

            // Send Push Notification
            if (!empty($data['employee'])) {
                foreach ($data['employee'] as $k => $employeeId) {
                    foreach (getUserFcmToken($employeeId) as $fcmToken):
                        $post = array(
                            "to" => $fcmToken->fcm_token,
                            "collapse_key" => "green",
                            "notification" => [
                                "title" => "Your have a task",
                                "body" => "$request->name",
                                "icon" => asset('/admin/images/logos/logo-icon.png'),
                                "image" => "",
                            ]
                        );
                        fcmFire($post);
                    endforeach;
                }
            }

        }


        if (!empty($data['employee'])) {
            $employeeDatas = [];
            foreach ($data['employee'] as $k => $employeeId) {
                $employeeDatas[] = User::findOrFail($employeeId);
            }
        }

        foreach ($employeeDatas as $employeeData){
            $email = $employeeData->email;
            Mail::send('emails.task', [
                'supervisor_name' => Auth::user()->name,
                'employee_name'   => $employeeData->name,
                'project_name'    => $task->project->project_name,
                'task_name'       => $request->name,
                'start_date'      => $request->start_date,
                'end_date'        => $request->end_date,
                'ratio'           => $numberDays,
            ], function ($msg) use ($email, $request)
                {
                    $msg->from('info@mediusware.com', 'MediusWare.Com');
                    $msg->subject('You have a new Task');
                    $msg->to($email);
                });
        }

        return redirect(route('task.index'))->with('success', 'Task Save Successfully');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = Task::find($id);
        $reviews = Review::where('task_id', $id)->orderBy('date', 'DESC')->get();
        $upReview = Review::where('task_id', $id)->where('date', date('Y-m-d'))->first();
        return view('admin.tasks.view', compact('task', 'reviews', 'upReview'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::find($id);
        $task->employees = $task->employees->pluck('employee_id')->toArray();
        $employees = User::where('role', 'employee')->orderBy('sort', 'ASC')->get();
        $projects = Project::get();
//        return $task;
        return view('admin.tasks.edit', compact('task', 'employees', 'projects'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'employee' => 'required',
            'name' => 'required|max:191',
            'task_details' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'status' => Rule::in(['active', 'deactivate', 'complete']),
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $startTimeStamp = strtotime($request->start_date);
        $endTimeStamp = strtotime($request->end_date);
        $timeDiff = abs($endTimeStamp - $startTimeStamp);
        $numberDays = $timeDiff/86400;  // 86400 seconds in one day
        $numberDays = 1 + intval($numberDays);

        $task = Task::find($id);
        $task->name         = $request->name;
        $task->project_id   = $request->project;
        $task->task_details = $request->task_details;
        $task->start_date   = $request->start_date;
        $task->end_date     = $request->end_date;
        $task->ratio        = $numberDays;
        $task->note         = $request->note;
        $task->status       = $request->status;
        if ($task->save()){
            if (!empty($data['employee'])) {
                TaskEmployeeRelation::where('task_id', $id)->delete();
                $employeeData = [];
                foreach ($data['employee'] as $k => $employeeId) {
                    $employeeData[] = [
                        'task_id' => $task->id,
                        'employee_id' => $employeeId,
                    ];
                }
                if (!empty($employeeData)) {
                    TaskEmployeeRelation::insert($employeeData);
                }
            }

            if (!empty($data['task_list'])) {
                //array filter for zero empty value check
                TaskList::where('task_id', $id)->delete();
                $task_lists = $data['task_list'];
                $task_lists = !empty($task_lists) ? array_values(array_filter($task_lists)) : array();
                $listData = [];
                foreach ($task_lists as $k => $task_list) {
                    $listData[] = [
                        'task_id'   => $task->id,
                        'list_name' => $task_list,
                    ];
                }
                if (!empty($listData)) {
                    TaskList::insert($listData);
                }
            }

            if($request->hasfile('images'))
            {
                $images = [];
                foreach($request->file('images') as $key => $image)
                {
                    $name = $image->getClientOriginalName();
                    $mainName = time().'_'. $name;
                    $image->move(public_path().'/media/task/',$mainName);
                    $images[] = [
                        'task_id' => $task->id,
                        'image' => $mainName,
                    ];
                }

                if (!empty($images)) {
                    MediaImage::insert($images);
                }
            }

        }
        return redirect(route('task.index'))->with('success', 'Task Update Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::find($id);
        TaskEmployeeRelation::where('task_id', $id)->delete();
        $images = MediaImage::where('task_id', $id)->get();
        if (!empty($images)){
            foreach ($images as $img){
                if ($img->image){
                    unlink(public_path('media/task/'.$img->image));
                }
                $img->delete();
            }
        }

        $task->delete();
        return redirect()->back()->with('error', 'Task Delete Successfully');
    }

}
