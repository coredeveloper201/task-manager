<?php

namespace App\Http\Controllers\Admin;

use App\Models\MediaImage;
use App\Models\Review;
use App\Models\Task;
use App\Models\TaskEmployeeRelation;
use App\Models\Work;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;
use Validator;

class WorkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employeeId = Auth::user()->id;
        $taskQueries = Task::where('tasks.status', 'active');
        if ($employeeId){
            $taskQueries->join('task_employee_relations as re_task', 'tasks.id', '=', 're_task.task_id');
            $taskQueries->join('users', 'users.id', '=', 're_task.employee_id');
            $taskQueries->select('tasks.*');
            $taskQueries->where('re_task.employee_id', '=', $employeeId);
        }
        $tasks = $taskQueries->get();
        return view('admin.works.index', compact('tasks'));
    }

    public function workList(){
        $user = Auth::user();
        if ($user->role == 'employee'){
            $works = Work::where('employee_id', $user->id)->orderBy('id', 'desc')->get();
        }else{
            $works = Work::orderBy('id', 'desc')->get();
        }

        return view('admin.works.work_list', compact('works'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($taskId = null)
    {
        return view('admin.works.create', compact('taskId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'task_id' => 'required',
            'work_details' => 'required',
            'status' => Rule::in(['pending', 'working', 'complete', 'rejected']),
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $work = new Work();
        $work->task_id           = $request->task_id;
        $work->employee_id       = Auth::user()->id;
        $work->done_task_details = $request->work_details;
        $work->note              = $request->note;
        $work->status            = $request->status;
        if ($work->save()){
            if($request->hasfile('images'))
            {
                $images = [];
                foreach($request->file('images') as $key => $image)
                {
                    $name = $image->getClientOriginalName();
                    $mainName = time().'_'. $name;
                    $image->move(public_path().'/media/task/',$mainName);
                    $images[] = [
                        'work_id' => $work->id,
                        'image' => $mainName,
                    ];
                }

                if (!empty($images)) {
                    MediaImage::insert($images);
                }
            }

            if ($request->status == 'complete' || $request->status == 'pending' || $request->status == 'working'){

                $workStatus = $request->status;
                $taskInfo = Task::find($work->task_id);
                $supervisor = User::find($taskInfo->super_vision);
                $employeeData = User::find($work->employee_id);
                $email = $supervisor->email;
                Mail::send('emails.work', [
                    'supervisor_name' => $supervisor->name,
                    'employee_name'   => $employeeData->name,
                    'task_name'       => $taskInfo->name,
                    'start_date'      => $taskInfo->start_date,
                    'end_date'        => $taskInfo->end_date,
                    'ratio'           => $taskInfo->ratio,
                    'work_details'    => $work->done_task_details,
                    'complete_date'   => date('F d, Y'),
                ], function ($msg) use ($email, $request, $workStatus)
                {
                    $msg->from('info@mediusware.com', 'MediusWare.Com');
                    $msg->subject('Task '. $workStatus.' Status');
                    $msg->to($email);
                });
            }

            // Send Push Notification
            foreach (getUserFcmToken($supervisor->id) as $fcmToken):
                $post = array(
                    "to" => $fcmToken->fcm_token,
                    "collapse_key" => "green",
                    "notification" => [
                        "title" => "$employeeData->name Work Update",
                        "body" => "$taskInfo->name",
                        "icon" => asset('/admin/images/logos/logo-icon.png'),
                        "image" => "",
                    ]
                );
                fcmFire($post);
            endforeach;

        }
        return redirect(route('work.index'))->with('success', 'Work Save Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $work = Work::find($id);
        $reviews = Review::where('work_id', $id)->orderBy('date', 'DESC')->get();
        $upReview = Review::where('work_id', $id)->where('date', date('Y-m-d'))->first();
//        return $upReview;
        return view('admin.works.view', compact('work', 'reviews', 'upReview'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employeeId = Auth::user()->id;
        $taskQueries = Task::where('tasks.status', 'active');
        if ($employeeId){
            $taskQueries->join('task_employee_relations as re_task', 'tasks.id', '=', 're_task.task_id');
            $taskQueries->join('users', 'users.id', '=', 're_task.employee_id');
            $taskQueries->select('tasks.id as task_id', 'tasks.name as task_name');
            $taskQueries->where('re_task.employee_id', '=', $employeeId);
        }
        $tasks = $taskQueries->get();
        $work = Work::find($id);
        return view('admin.works.edit', compact('tasks' ,'work'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'task_id' => 'required',
            'work_details' => 'required',
            'status' => Rule::in(['pending', 'working', 'complete', 'rejected']),
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $work = Work::find($id);
        $work->task_id           = $request->task_id;
        $work->done_task_details = $request->work_details;
        $work->note              = $request->note;
        $work->status            = $request->status;
        if ($work->save()){
            if($request->hasfile('images'))
            {
                $images = [];
                foreach($request->file('images') as $key => $image)
                {
                    $name = $image->getClientOriginalName();
                    $mainName = time().'_'. $name;
                    $image->move(public_path().'/media/task/',$mainName);
                    $images[] = [
                        'work_id' => $work->id,
                        'image' => $mainName,
                    ];
                }

                if (!empty($images)) {
                    MediaImage::insert($images);
                }
            }

            if ($request->status == 'complete' || $request->status == 'pending' || $request->status == 'working'){
                $workStatus = $request->status;
                $taskInfo = Task::find($work->task_id);
                $supervisor = User::find($taskInfo->super_vision);
                $employeeData = User::find($work->employee_id);
                $email = $supervisor->email;
                Mail::send('emails.work', [
                    'supervisor_name' => $supervisor->name,
                    'employee_name'   => $employeeData->name,
                    'task_name'       => $taskInfo->name,
                    'start_date'      => $taskInfo->start_date,
                    'end_date'        => $taskInfo->end_date,
                    'ratio'           => $taskInfo->ratio,
                    'work_details'    => $work->done_task_details,
                    'complete_date'   => date('F d, Y'),
                ], function ($msg) use ($email, $request, $workStatus)
                {
                    $msg->from('info@mediusware.com', 'MediusWare.Com');
                    $msg->subject('Task '. $workStatus.' Status');
                    $msg->to($email);
                });
            }
            // Send Push Notification
            foreach (getUserFcmToken($supervisor->id) as $fcmToken):
                $post = array(
                    "to" => $fcmToken->fcm_token,
                    "collapse_key" => "green",
                    "notification" => [
                        "title" => "$employeeData->name Work Update",
                        "body" => "$taskInfo->name",
                        "icon" => asset('/admin/images/logos/logo-icon.png'),
                        "image" => "",
                    ]
                );
                fcmFire($post);
            endforeach;

        }
        return redirect(route('work.index'))->with('success', 'Work Update Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $work = Work::find($id);
        $images = MediaImage::where('work_id', $id)->get();
        if (!empty($images)){
            foreach ($images as $img){
                if ($img->image){
                    unlink(public_path('media/task/'.$img->image));
                }
                $img->delete();
            }
        }
        $work->delete();
        return redirect(route('work.index'))->with('error', 'Work Delete Successfully');
    }
}
