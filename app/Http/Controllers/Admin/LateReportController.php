<?php

namespace App\Http\Controllers\Admin;

use App\Models\LateReport;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Auth;

class LateReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('role', 'employee')->where('status', 'active')->orderBy('sort', 'ASC')->get();
        return view('admin.late_report.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        if (!empty($data['in_time'])) {
            $lateReport = [];
            foreach ($data['in_time'] as $k => $item) {
                $dateCheck = LateReport::where('date', $data['date'])->where('user_id', $k)->first();
                if ($dateCheck){
                    return redirect()->back()->with('error', 'This date already late report inserted!');
                }elseif($data['date'] > date('Y-m-d')){
                    return redirect()->back()->with('error', 'Invalid your date!');
                }
                $lateReport[] = [
                    'user_id' => $k,
                    'date'    => $data['date'],
                    'in_time' => $item,
                    'remarks' => $data['remarks'][$k],
                ];
            }
            if (!empty($lateReport)) {
                LateReport::insert($lateReport);
            }
        }else{
            return redirect()->back()->with('error', 'Employee not selected!');
        }

        return redirect()->back()->with('success', 'Report save successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function reportList(){
        $lateReports = LateReport::with('user')->whereHas('user', function($q){
            $q->where('status', 'active');
        })->get();
        $users = User::where('role', 'employee')->where('status', 'active')->get();
        $data = [];
        return view('admin.late_report.list', compact('lateReports', 'users', 'data'));
    }

    public function filter( Request $request){
        if (Auth::user()->role == 'employee'){
            $data = $request->all();
            $lateReports = LateReport::where('user_id', Auth::user()->id)
                ->whereBetween('date', [$data['start_date'], $data['end_date']])
                ->orderBy('date', 'ASC')
                ->get();
            return view('admin.late_report.emp_list', compact('lateReports', 'users', 'data'));
        }else{
            $users = User::where('role', 'employee')->get();
            $data = $request->all();
            if ($data['user_id'] == 'all'){
                $lateReports = LateReport::whereBetween('date', [$data['start_date'], $data['end_date']])
                    ->orderBy('date', 'ASC')
                    ->get();
            }else {
                $lateReports = LateReport::where('user_id', $data['user_id'])
                    ->whereBetween('date', [$data['start_date'], $data['end_date']])
                    ->orderBy('date', 'ASC')
                    ->get();
            }
        }

        return view('admin.late_report.list', compact('lateReports', 'users', 'data'));
    }

    public function empReportList(){
        $lateReports = LateReport::where('user_id', Auth::user()->id)->orderBy('date', 'desc')->get();
        $data = [];
        return view('admin.late_report.emp_list', compact('lateReports', 'users', 'data'));
    }

    public function monthlyReportList(Request $request){
        $year = !empty($request->year) ? $request->year:date('Y') ;
        $month = !empty($request->month) ? $request->month:date('m') ;

        $sql = '';
        if ($year) {
            $sql .= ' AND YEAR(date)='.$year;
        }
        if ($month) {
            $sql .= ' AND MONTH(date)='.$month;
        }
        $users = User::where('users.role', 'employee')->where('users.status', 'active');
        $users->select('users.id','users.name','users.image', 'B.in_time_12', 'C.in_time_11', 'D.in_time_10');
        $users->leftJoin(\DB::raw("(SELECT COUNT(id) as in_time_12, user_id FROM late_reports WHERE in_time='12' $sql GROUP BY user_id) AS B"), 'users.id', '=', 'B.user_id');
        $users->leftJoin(\DB::raw("(SELECT COUNT(id) as in_time_11, user_id FROM late_reports WHERE in_time='11_12' $sql GROUP BY user_id) AS C"), 'users.id', '=', 'C.user_id');
        $users->leftJoin(\DB::raw("(SELECT COUNT(id) as in_time_10, user_id FROM late_reports WHERE in_time='10_11' $sql GROUP BY user_id) AS D"), 'users.id', '=', 'D.user_id');

        $data = $users->get();
        return view('admin.late_report.monthly_report', compact('data', 'year', 'month'));
    }

    public function empMonthlyReportList(Request $request){
        $year = !empty($request->year) ? $request->year:date('Y') ;
        $month = !empty($request->month) ? $request->month:date('m') ;

        $sql = '';
        if ($year) {
            $sql .= ' AND YEAR(date)='.$year;
        }
        if ($month) {
            $sql .= ' AND MONTH(date)='.$month;
        }
        $users = User::where('users.id', Auth::user()->id);
        $users->select('users.id','users.name','users.image', 'B.in_time_12', 'C.in_time_11', 'D.in_time_10');
        $users->leftJoin(\DB::raw("(SELECT COUNT(id) as in_time_12, user_id FROM late_reports WHERE in_time='12' $sql GROUP BY user_id) AS B"), 'users.id', '=', 'B.user_id');
        $users->leftJoin(\DB::raw("(SELECT COUNT(id) as in_time_11, user_id FROM late_reports WHERE in_time='11_12' $sql GROUP BY user_id) AS C"), 'users.id', '=', 'C.user_id');
        $users->leftJoin(\DB::raw("(SELECT COUNT(id) as in_time_10, user_id FROM late_reports WHERE in_time='10_11' $sql GROUP BY user_id) AS D"), 'users.id', '=', 'D.user_id');

        $data = $users->get();
        return view('admin.late_report.emp_monthly_report', compact('data', 'year', 'month'));
    }
}
