<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Question;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ExamQuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'exam_id' => 'required',
            'question' => 'required',
            'image' => 'mimes:jpeg,jpg,png,gif|max:10000',
        ));
        if ($validator->fails()) {
            return response()->json(['success' => false, 'error' => $validator->errors(), 'status' => 400], 200);
        }

        $data = array(
            'exam_id' => $request->exam_id,
            'question' => $request->question,
            'description' => $request->description,
        );
        if ($request->hasFile('image')) {
            $upload = uploadFile($request->image, 'exam');
            if ($upload) {
                $data['image'] = $upload;
            } else {
                return response()->json(['success' => false, 'error' => 'Whoops! Failed to upload image', 'status' => 400], 200);
            }
        }

        $question = Question::create($data);
        if ($question) {
            return response()->json(['success' => true, 'error' => $question, 'status' => 400], 200);
        } else {
            return response()->json(['success' => false, 'error' => 'Whoops! Question Not Created', 'status' => 400], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $question = Question::find($id);
        if ($question) {
            return response()->json(['success' => true, 'data' => $question, 'status' => 200], 200);
        } else {
            return response()->json(['success' => false, 'data' => 'Question Not Found', 'status' => 401], 200);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), array(
            'question' => 'required',
            'image' => 'mimes:jpeg,jpg,png,gif|max:10000',
        ));
        if ($validator->fails()) {
            return response()->json(['success' => false, 'error' => $validator->errors(), 'status' => 400], 200);
        }

        $data = array(
            'question' => $request->question,
            'description' => $request->description,
        );
        $question = Question::find($id);
        if ($request->hasFile('image')) {
            $path = public_path("exam/$question->image");
            File::delete($path);
            $upload = uploadFile($request->image, 'exam');
            if ($upload) {
                $data['image'] = $upload;
            } else {
                return response()->json(['success' => false, 'error' => 'Whoops! Failed to upload image', 'status' => 400], 200);
            }
        }
        $question = $question->update($data);
        if ($question) {
            return response()->json(['success' => true, 'data' => $question, 'status' => 200], 200);
        } else {
            return response()->json(['success' => false, 'Whoops! Question not updated', 'status' => 401], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $question = Question::find($id);
        $path = public_path("exam/$question->image");
        File::delete($path);
        $destroy = $question->delete();
        if ($destroy) {
            return response()->json(['success' => true, 'Question deleted Successfully', 'status' => 200], 200);
        } else {
            return response()->json(['success' => false, 'Whoops! Question not deleted', 'status' => 401], 200);
        }
    }

    public function deleteImage($id)
    {
        $question = Question::find($id);
        $path = public_path("exam/$question->image");
        File::delete($path);
        $update = $question->update(['image' => null]);
        if ($update) {
            return response()->json(['success' => true, 'data' => $update, 'status' => 200], 200);
        } else {
            return response()->json(['success' => false, 'Whoops! Failed to remove image', 'status' => 401], 200);
        }
    }
}
