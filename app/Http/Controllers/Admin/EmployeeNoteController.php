<?php

namespace App\Http\Controllers\Admin;

use App\Models\EmployeeNote;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class EmployeeNoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = EmployeeNote::orderBy('id', 'desc')->get();
        return view('admin.emp-note.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::where('role', '!=', 'admin')->where('status', 'active')->orderBy('id', 'desc')->get();
        $data = null;
        return view('admin.emp-note.create', compact('users', 'data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'date' => 'required|date',
            'user_id' => 'required|numeric',
            'comment' => 'required',
        ], [
            'user_id.numeric' => "This user is invalid"
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        EmployeeNote::create($data);
        return redirect()->back()->with('success', 'Employee note save successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = EmployeeNote::find($id);
        return view('admin.emp-note.view', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = User::where('role', '!=', 'admin')->where('status', 'active')->orderBy('id', 'desc')->get();
        $data = EmployeeNote::find($id);
        return view('admin.emp-note.edit', compact('users', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'date' => 'required|date',
            'user_id' => 'required|numeric',
            'comment' => 'required',
        ], [
            'user_id.numeric' => "This user is invalid"
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        EmployeeNote::find($id)->update($data);
        return redirect()->back()->with('success', 'Employee note update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        EmployeeNote::find($id)->delete();
        return redirect()->back()->with('success', 'Employee note delete successfully');
    }
}
