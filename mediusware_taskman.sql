-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 12, 2019 at 02:07 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mediusware_taskman`
--

-- --------------------------------------------------------

--
-- Table structure for table `fcm_tokens`
--

CREATE TABLE `fcm_tokens` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `fcm_token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fcm_tokens`
--

INSERT INTO `fcm_tokens` (`id`, `user_id`, `fcm_token`, `created_at`, `updated_at`) VALUES
(4, 4, 'fe2VQqujg_0:APA91bGkEz7JW-tdtTPPDAUDT-0ZfMr1IDU_enlAaKnooJ_74FqrKqe0uYZL7QzrPhBjNIPLSfgPlJ_1kVsqffKsYFBqi66aOaESz7RSanaUZXB3rvomsSgsKaryiC36Izp4jc_VtK2W', '2019-09-11 23:50:28', '2019-09-11 23:50:28'),
(5, 3, 'eVfetUg0eoE:APA91bElV5vj8cqib-guavg-vKyb34bCi6jOjbsJeW2_Fiw9reZMiKL_CONDwHx2kF9yHt1m8A97scm2B2n35If1S-eGCWnE-T2gNtFXNWXvAWvKe_Kea2E_BQYpaTfjcC8V62rvvnMB', '2019-09-11 23:50:51', '2019-09-11 23:50:51'),
(6, 6, 'eVfetUg0eoE:APA91bElV5vj8cqib-guavg-vKyb34bCi6jOjbsJeW2_Fiw9reZMiKL_CONDwHx2kF9yHt1m8A97scm2B2n35If1S-eGCWnE-T2gNtFXNWXvAWvKe_Kea2E_BQYpaTfjcC8V62rvvnMB', '2019-09-12 05:08:16', '2019-09-12 05:08:16');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `task_id` int(11) DEFAULT NULL,
  `work_id` int(11) DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','deactivate') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `sort` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `task_id`, `work_id`, `image`, `status`, `sort`, `created_at`, `updated_at`) VALUES
(4, 1, NULL, '1565162652_share link notifications.png', 'active', 0, NULL, NULL),
(5, 1, NULL, '1565162652_verify email.png', 'active', 0, NULL, NULL),
(7, NULL, 1, '1565177031_verify email.png', 'active', 0, NULL, NULL),
(8, NULL, 1, '1565180125_verify email.png', 'active', 0, NULL, NULL),
(9, 3, NULL, '1565180637_login notifications.png', 'active', 0, NULL, NULL),
(10, NULL, 2, '1565239864_screencapture-127-0-0-1-8000-mediuswareadmin-feature-2019-07-02-18_20_37.png', 'active', 0, NULL, NULL),
(12, NULL, 2, '1565239864_screencapture-127-0-0-1-8000-mediuswareadmin-portfolios-2019-07-02-11_33_16.png', 'active', 0, NULL, NULL),
(14, 5, NULL, '1565265837_screencapture-127-0-0-1-8000-features-2019-07-04-13_09_18.png', 'active', 0, NULL, NULL),
(15, 5, NULL, '1565265837_screencapture-127-0-0-1-8000-features-2019-07-04-18_19_26.png', 'active', 0, NULL, NULL),
(16, 5, NULL, '1565265837_screencapture-127-0-0-1-8000-mediuswareadmin-about-2019-07-12-19_08_11.png', 'active', 0, NULL, NULL),
(28, 9, NULL, '1566195281_Screenshot_1.png', 'active', 0, NULL, NULL),
(29, 9, NULL, '1566195281_Screenshot_2.png', 'active', 0, NULL, NULL),
(30, 9, NULL, '1566195281_Screenshot_3.png', 'active', 0, NULL, NULL),
(31, 9, NULL, '1566195281_Screenshot_4.png', 'active', 0, NULL, NULL),
(32, 10, NULL, '1566195591_Screenshot_1.png', 'active', 0, NULL, NULL),
(33, 10, NULL, '1566195591_Screenshot_2.png', 'active', 0, NULL, NULL),
(34, 11, NULL, '1566362035_picturemessage_fvl4uxr3.1vj.png', 'active', 0, NULL, NULL),
(35, NULL, 3, '1566362588_Screenshot_1.png', 'active', 0, NULL, NULL),
(36, NULL, 3, '1566362588_Screenshot_2.png', 'active', 0, NULL, NULL),
(37, NULL, 3, '1566362588_Screenshot_3.png', 'active', 0, NULL, NULL),
(38, NULL, 3, '1566362588_Screenshot_4.png', 'active', 0, NULL, NULL),
(39, NULL, 4, '1566378488_Screenshot_1.png', 'active', 0, NULL, NULL),
(40, NULL, 4, '1566378488_Screenshot_2.png', 'active', 0, NULL, NULL),
(41, NULL, 4, '1566378488_Screenshot_3.png', 'active', 0, NULL, NULL),
(42, NULL, 4, '1566378488_Screenshot_4.png', 'active', 0, NULL, NULL),
(43, 12, NULL, '1568202162_favicon.png', 'active', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `late_reports`
--

CREATE TABLE `late_reports` (
  `id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `in_time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `late_reports`
--

INSERT INTO `late_reports` (`id`, `user_id`, `date`, `in_time`, `remarks`, `created_at`, `updated_at`) VALUES
(9, 5, '2019-09-04', '11_12', 'aadf', NULL, NULL),
(10, 4, '2019-09-06', '12', 'sfdg', NULL, NULL),
(14, 4, '2019-09-01', '11_12', 'asdf asfd sadf', NULL, NULL),
(17, 5, '2019-09-01', '10_11', 'asdfa df asdf', NULL, NULL),
(18, 5, '2019-09-01', '10_11', 'asdfasdf', NULL, NULL),
(19, 5, '2019-09-01', '10_11', 'adsf', NULL, NULL),
(20, 5, '2019-09-07', '10_11', 'asdf', NULL, NULL),
(21, 4, '2019-09-07', '10_11', 'asdf', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `leaves`
--

CREATE TABLE `leaves` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `casual_leave` int(11) NOT NULL,
  `sick_leave` int(11) NOT NULL,
  `year` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `leaves`
--

INSERT INTO `leaves` (`id`, `user_id`, `casual_leave`, `sick_leave`, `year`, `created_at`, `updated_at`) VALUES
(1, 3, 14, 7, 2019, '2019-09-11 04:56:32', '2019-09-12 00:21:03'),
(2, 4, 14, 7, 2019, '2019-09-11 04:56:32', '2019-09-12 00:21:03'),
(3, 5, 14, 7, 2019, '2019-09-11 04:56:32', '2019-09-12 00:21:03'),
(4, 6, 14, 7, 2019, '2019-09-11 04:56:32', '2019-09-12 00:21:03');

-- --------------------------------------------------------

--
-- Table structure for table `leave_applications`
--

CREATE TABLE `leave_applications` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `application` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `leave_type` enum('casual_leave','sick_leave') COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `date_count` tinyint(4) NOT NULL DEFAULT '0',
  `status` enum('approved','rejected','pending') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `isApproved` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `leave_applications`
--

INSERT INTO `leave_applications` (`id`, `user_id`, `subject`, `application`, `leave_type`, `date_from`, `date_to`, `date_count`, `status`, `isApproved`, `created_at`, `updated_at`) VALUES
(1, 3, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do</p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do.<br></p>', 'sick_leave', '2019-09-11', '2019-09-12', 2, 'approved', 1, '2019-09-11 00:21:40', '2019-09-12 00:28:04'),
(2, 3, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do', '<p><p style=\"margin-right: 0px; margin-bottom: 3rem; margin-left: 0px; color: rgb(43, 43, 43); font-family: Lato; font-size: 16px;\"><span style=\"font-weight: 700;\">Dear [Supervisor’s full name],</span></p><p style=\"margin-right: 0px; margin-bottom: 3rem; margin-left: 0px; color: rgb(43, 43, 43); font-family: Lato; font-size: 16px;\">I would like to request [Number Of Days] days leave due to [State Reason]. If possible, I would prefer to leave as soon as on [Date Leave Starts] and return back on [Date Leave Ends].</p><p style=\"margin-right: 0px; margin-bottom: 3rem; margin-left: 0px; color: rgb(43, 43, 43); font-family: Lato; font-size: 16px;\">I shall be very grateful if this request can be approved straightaway as it is of some urgency to me. </p></p>', 'casual_leave', '2019-09-11', '2019-09-11', 1, 'approved', 1, '2019-09-11 05:51:35', '2019-09-12 03:54:15'),
(3, 3, 'asdf asfd', '<p><p style=\"margin-right: 0px; margin-bottom: 3rem; margin-left: 0px; color: rgb(43, 43, 43); font-family: Lato; font-size: 16px;\"><span style=\"font-weight: 700;\">Dear [Supervisor’s full name],</span></p><p style=\"margin-right: 0px; margin-bottom: 3rem; margin-left: 0px; color: rgb(43, 43, 43); font-family: Lato; font-size: 16px;\">I would like to request [Number Of Days] days leave due to [State Reason]. If possible, I would prefer to leave as soon as on [Date Leave Starts] and return back on [Date Leave Ends]. <br></p><p style=\"margin-right: 0px; margin-bottom: 3rem; margin-left: 0px; color: rgb(43, 43, 43); font-family: Lato; font-size: 16px;\">I shall be very grateful if this request can be approved straightaway as it is of some urgency to me.</p></p>', 'sick_leave', '2019-09-14', '2019-09-16', 3, 'rejected', 1, '2019-09-12 00:10:56', '2019-09-12 03:58:57');

-- --------------------------------------------------------

--
-- Table structure for table `leave_templates`
--

CREATE TABLE `leave_templates` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `template` longtext NOT NULL,
  `status` enum('active','deactivate') NOT NULL DEFAULT 'deactivate',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `leave_templates`
--

INSERT INTO `leave_templates` (`id`, `name`, `template`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Leave of absence from work', '<p style=\"margin-right: 0px; margin-bottom: 3rem; margin-left: 0px; color: rgb(43, 43, 43); font-family: Lato; font-size: 16px;\"><span style=\"font-weight: 700;\">Dear [Supervisor’s full name],</span></p><p style=\"margin-right: 0px; margin-bottom: 3rem; margin-left: 0px; color: rgb(43, 43, 43); font-family: Lato; font-size: 16px;\">I would like to request [Number Of Days] days leave due to [State Reason]. If possible, I would prefer to leave as soon as on [Date Leave Starts] and return back on [Date Leave Ends].</p><p style=\"margin-right: 0px; margin-bottom: 3rem; margin-left: 0px; color: rgb(43, 43, 43); font-family: Lato; font-size: 16px;\">I shall be very grateful if this request can be approved straightaway as it is of some urgency to me.</p>', 'active', '2019-09-11 04:00:52', '2019-09-11 04:00:52'),
(2, 'Leave of absence due to fever', '<p style=\"margin-right: 0px; margin-bottom: 3rem; margin-left: 0px; color: rgb(43, 43, 43); font-family: Lato; font-size: 16px;\"><span style=\"font-weight: 700;\">To whom it may concern,</span></p><p style=\"margin-right: 0px; margin-bottom: 3rem; margin-left: 0px; color: rgb(43, 43, 43); font-family: Lato; font-size: 16px;\">I beg to let you know that I am down with high fever. As a result, I will be unable to attend classes / work from [Date Leave Starts] to [Date Leave Ends]. I will make a special effort to seek medical advice and will get back as soon as possible.</p><p style=\"margin-right: 0px; margin-bottom: 3rem; margin-left: 0px; color: rgb(43, 43, 43); font-family: Lato; font-size: 16px;\">Kindly approve me leave of absence for [Number Of Days].</p><p style=\"margin-right: 0px; margin-bottom: 3rem; margin-left: 0px; color: rgb(43, 43, 43); font-family: Lato; font-size: 16px;\"><span style=\"font-weight: 700;\">Yours faithfully,</span></p><p style=\"margin-right: 0px; margin-bottom: 3rem; margin-left: 0px; color: rgb(43, 43, 43); font-family: Lato; font-size: 16px;\"><span style=\"font-weight: 700;\">[Your Full Name]</span></p>', 'active', '2019-09-11 04:02:42', '2019-09-11 04:02:42');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_09_06_121102_create_late_reports_table', 2),
(4, '2019_09_09_092738_create_leaves_table', 3),
(5, '2019_09_09_093645_create_leave_applications_table', 3),
(6, '2019_09_11_123324_create_fcm_tokens_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `ratio` tinyint(4) DEFAULT NULL,
  `status` enum('active','deactivate') NOT NULL DEFAULT 'deactivate',
  `sort` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `project_name`, `description`, `start_date`, `end_date`, `ratio`, `status`, `sort`, `created_at`, `updated_at`) VALUES
(2, 'Equidesk', '<p>Work smarter, not harder. Equidesk’s work order &amp; FM software simplifies your work management, personnel, invoicing and much more. Spend more time on growing your business and less time worrying.<br></p>', '2019-08-20', '2019-09-14', 26, 'active', 0, '2019-08-18 22:51:22', '2019-08-18 22:51:22'),
(3, 'WooStudy', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s<br></p>', '2019-07-01', '2019-09-30', 92, 'active', 0, '2019-08-18 22:52:50', '2019-08-18 22:52:50'),
(4, 'Testing', '<p>sdkbkv sdkc vsdk cfsd</p>', '2019-09-11', '2019-09-13', 3, 'active', 0, '2019-09-11 05:41:48', '2019-09-11 05:41:48');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `task_id` int(11) DEFAULT NULL,
  `work_id` int(11) DEFAULT NULL,
  `rating` enum('1','2','3','4','5') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comments` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `employee_id`, `task_id`, `work_id`, `rating`, `comments`, `date`, `sort`, `created_at`, `updated_at`) VALUES
(4, 4, NULL, 1, '3', 'asdf', '2019-08-07', 0, '2019-08-08 01:51:36', '2019-08-08 03:26:41'),
(5, 5, NULL, 2, '3', 'asd asdf', '2019-08-07', 0, '2019-08-08 03:00:04', '2019-08-08 03:00:04'),
(6, 4, NULL, 1, '5', 'Problem Solving Problem Solving Problem', '2019-08-08', 0, '2019-08-08 03:27:45', '2019-08-08 03:27:45'),
(7, NULL, 2, NULL, '3', 'New Problem New Problem New Problem New', '2019-08-08', 0, '2019-08-08 04:13:42', '2019-08-08 04:13:42'),
(8, 5, NULL, 2, '5', 'daddy ad dado edge Dec def', '2019-08-19', 0, '2019-08-19 02:23:14', '2019-08-19 02:23:47'),
(9, 4, NULL, 3, '5', 'Excellent', '2019-08-21', 0, '2019-08-20 22:57:08', '2019-08-20 22:57:08');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `super_vision` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `task_details` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `ratio` tinyint(4) DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `status` enum('active','deactivate','complete') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'deactivate',
  `sort` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `project_id`, `super_vision`, `name`, `task_details`, `start_date`, `end_date`, `ratio`, `note`, `status`, `sort`, `created_at`, `updated_at`) VALUES
(1, 3, 3, 'Problem Solving', '<p>Problem Solving Problem Solving Problem Solving Problem Solving Problem Solving Problem Solving <br></p>', '2019-08-07', '2019-08-10', 4, 'Problem Solving Problem Solving Problem Solving Problem Solving Problem Solving Problem Solving \r\nProblem Solving Problem Solving Problem Solving Problem Solving Problem Solving Problem Solving', 'complete', 0, '2019-08-07 00:22:19', '2019-08-19 03:49:47'),
(2, NULL, 3, 'New Problem', '<p>New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;</p><p>New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;</p><p>New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;New Problem&nbsp;<br></p>', '2019-08-07', '2019-08-07', 1, 'New Problem New Problem New Problem New Problem New Problem New Problem New Problem New Problem New Problem', 'active', 0, '2019-08-07 04:58:22', '2019-08-07 04:58:22'),
(3, NULL, 3, 'New Data', '<p>New Problem New Problem New Problem New Problem New Problem New Problem <br></p>', '2019-08-07', '2019-08-17', 11, 'New Problem New Problem New Problem New Problem New Problem', 'deactivate', 0, '2019-08-07 04:58:51', '2019-08-07 06:23:57'),
(4, NULL, 3, 'Mediusware', '<p>adsfasdf</p>', '2019-08-08', '2019-08-08', 1, 'asdaf', 'active', 0, '2019-08-08 05:55:17', '2019-08-08 05:55:17'),
(5, NULL, 3, 'Mediusware Taskman problem solving.', '<p>&nbsp;alksdjf asldkjf alsdkjf laksdjf lkasdjf laksdjf lkasdjf&nbsp;</p>', '2019-08-08', '2019-08-11', 4, 'alksdjf asldkjf alsdkjf laksdjf lkasdjf laksdjf lkasdjf', 'active', 0, '2019-08-08 06:03:57', '2019-08-08 06:03:57'),
(9, 2, 3, 'Real Time Notification', '<p><span style=\"color: rgb(80, 102, 143); font-family: none; font-size: 22px; font-style: italic; text-align: justify;\">Work smarter, not harder. Equidesk’s work order &amp; FM software simplifies your work management, personnel, invoicing and much more. Spend more time on growing your business and less time worrying</span><br></p>', '2019-08-19', '2019-08-23', 5, NULL, 'active', 0, '2019-08-19 00:14:41', '2019-08-19 00:14:41'),
(10, 2, 3, 'Xuher Anzum', '<p>asdf asdf asdf</p>', '2019-08-19', '2019-08-19', 1, NULL, 'active', 0, '2019-08-19 00:19:51', '2019-08-19 00:19:51'),
(11, 3, 6, 'Problem Solving Next Post', '<ul class=\"step-text\" style=\"margin-bottom: 10px; list-style: none; padding-left: 0px; counter-reset: b 0; color: rgb(51, 51, 51); font-family: Lato, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 16px;\"><li style=\"position: relative; padding-left: 60px; margin-bottom: 48px;\"><h5 style=\"font-family: Raleway, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; line-height: 1.1; margin-top: 10px; margin-bottom: 10px; font-size: 18px; padding-top: 5px;\">1. Upload archive</h5><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px;\">Upload the downloaded zip archive which contains all necessary files of app to any directory in your web hosting.</p></li><li style=\"position: relative; padding-left: 60px; margin-bottom: 48px;\"><h5 style=\"font-family: Raleway, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; line-height: 1.1; margin-top: 10px; margin-bottom: 10px; font-size: 18px; padding-top: 5px;\">2. Extract files</h5><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px;\">Extract all files from archive file to the directory where your application will be installed. This directory might be root directory of your domain (i.e. public_html or www) or any public accessible subdirectory of any domain.</p><div class=\"text-gray\" style=\"color: rgb(204, 204, 204);\">Please note that some hosting providers don\'t allow to extract&nbsp;<code style=\"font-size: 14.4px; font-family: Menlo, Monaco, Consolas, &quot;Courier New&quot;, monospace; border-radius: 4px; padding: 2px 4px; color: rgb(244, 100, 95); background-color: rgb(240, 242, 241); text-shadow: rgb(255, 255, 255) 0px 1px;\">.htaccess</code>&nbsp;files from zip archive. There are two&nbsp;<code style=\"font-size: 14.4px; font-family: Menlo, Monaco, Consolas, &quot;Courier New&quot;, monospace; border-radius: 4px; padding: 2px 4px; color: rgb(244, 100, 95); background-color: rgb(240, 242, 241); text-shadow: rgb(255, 255, 255) 0px 1px;\">.htaccess</code>&nbsp;files in archive file. One of them in&nbsp;<code style=\"font-size: 14.4px; font-family: Menlo, Monaco, Consolas, &quot;Courier New&quot;, monospace; border-radius: 4px; padding: 2px 4px; color: rgb(244, 100, 95); background-color: rgb(240, 242, 241); text-shadow: rgb(255, 255, 255) 0px 1px;\">root directory</code>, another in&nbsp;<code style=\"font-size: 14.4px; font-family: Menlo, Monaco, Consolas, &quot;Courier New&quot;, monospace; border-radius: 4px; padding: 2px 4px; color: rgb(244, 100, 95); background-color: rgb(240, 242, 241); text-shadow: rgb(255, 255, 255) 0px 1px;\">/app</code>&nbsp;directory. Please make sure both of these files has been extracted.</div></li><li style=\"position: relative; padding-left: 60px; margin-bottom: 48px;\"><h5 style=\"font-family: Raleway, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; line-height: 1.1; margin-top: 10px; margin-bottom: 10px; font-size: 18px; padding-top: 5px;\">3. Navigate to installation page</h5><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px;\">Open your web browser and navigate to the directory you’ve selected in step 2. For example if you’ve extracted the application files to the root of your domain then you should navigate to the&nbsp;<span style=\"font-weight: 700;\">yourdomain.com</span>. When you navigate to the right directory then page should be redirected to installation page.</p></li></ul>', '2019-08-22', '2019-08-30', 9, 'New Task', 'active', 0, '2019-08-20 22:33:55', '2019-08-20 22:33:55'),
(12, 4, 3, 'ASDF', '<p>sdfgvsdj cvsjfdjk fvsd 2</p>', '2019-09-11', '2019-09-12', 2, 'note', 'active', 0, '2019-09-11 05:42:42', '2019-09-11 05:42:42'),
(13, 4, 3, 'New Task', '<p>&nbsp;aksjdfh kajsd fkjahd fkjhasdkfj aksdjf&nbsp;</p>', '2019-09-12', '2019-09-16', 5, 'asdfa', 'active', 0, '2019-09-12 04:33:07', '2019-09-12 04:33:07');

-- --------------------------------------------------------

--
-- Table structure for table `task_employee_relations`
--

CREATE TABLE `task_employee_relations` (
  `id` bigint(20) NOT NULL,
  `employee_id` bigint(20) NOT NULL,
  `task_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `task_employee_relations`
--

INSERT INTO `task_employee_relations` (`id`, `employee_id`, `task_id`, `created_at`, `updated_at`) VALUES
(8, 5, 2, '2019-08-07 10:58:22', '2019-08-07 10:58:22'),
(9, 4, 2, '2019-08-07 10:58:22', '2019-08-07 10:58:22'),
(11, 5, 3, '2019-08-07 12:23:57', '2019-08-07 12:23:57'),
(12, 5, 4, '2019-08-08 11:55:17', '2019-08-08 11:55:17'),
(13, 4, 4, '2019-08-08 11:55:17', '2019-08-08 11:55:17'),
(14, 4, 5, '2019-08-08 12:03:57', '2019-08-08 12:03:57'),
(24, 4, 9, '2019-08-19 06:14:41', '2019-08-19 06:14:41'),
(25, 4, 10, '2019-08-19 06:19:51', '2019-08-19 06:19:51'),
(26, 4, 1, '2019-08-19 09:49:47', '2019-08-19 09:49:47'),
(27, 4, 11, '2019-08-21 04:33:55', '2019-08-21 04:33:55'),
(28, 4, 12, '2019-09-11 11:42:42', '2019-09-11 11:42:42'),
(29, 5, 13, '2019-09-12 10:33:07', '2019-09-12 10:33:07'),
(30, 4, 13, '2019-09-12 10:33:07', '2019-09-12 10:33:07');

-- --------------------------------------------------------

--
-- Table structure for table `task_lists`
--

CREATE TABLE `task_lists` (
  `id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `list_name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `task_lists`
--

INSERT INTO `task_lists` (`id`, `task_id`, `list_name`, `created_at`, `updated_at`) VALUES
(7, 6, 'asd ads fasdf', '2019-08-19 05:11:20', '2019-08-19 05:11:20'),
(8, 6, 'dd decdxa', '2019-08-19 05:11:20', '2019-08-19 05:11:20'),
(9, 6, 'adfasd', '2019-08-19 05:11:20', '2019-08-19 05:11:20'),
(10, 7, 'Work Management System', '2019-08-19 06:03:54', '2019-08-19 06:03:54'),
(11, 7, 'Contact management', '2019-08-19 06:03:54', '2019-08-19 06:03:54'),
(12, 7, 'Asset management', '2019-08-19 06:03:54', '2019-08-19 06:03:54'),
(13, 7, 'Recurring System with Cron Job', '2019-08-19 06:03:54', '2019-08-19 06:03:54'),
(14, 8, 'Recurring System with Cron Job', '2019-08-19 06:14:21', '2019-08-19 06:14:21'),
(15, 8, 'Invoice Management', '2019-08-19 06:14:21', '2019-08-19 06:14:21'),
(16, 8, 'Location Facility management', '2019-08-19 06:14:21', '2019-08-19 06:14:21'),
(17, 9, 'Recurring System with Cron Job', '2019-08-19 06:14:41', '2019-08-19 06:14:41'),
(18, 9, 'Invoice Management', '2019-08-19 06:14:41', '2019-08-19 06:14:41'),
(19, 9, 'Location Facility management', '2019-08-19 06:14:41', '2019-08-19 06:14:41'),
(20, 10, 'Recurring System with Cron Job', '2019-08-19 06:19:51', '2019-08-19 06:19:51'),
(21, 10, 'Location Facility management', '2019-08-19 06:19:51', '2019-08-19 06:19:51'),
(22, 1, 'asd ads fasdf', '2019-08-19 09:49:47', '2019-08-19 09:49:47'),
(23, 11, 'Location Facility management', '2019-08-21 04:33:55', '2019-08-21 04:33:55'),
(24, 11, 'Recurring System with Cron Job', '2019-08-21 04:33:55', '2019-08-21 04:33:55'),
(25, 12, '1', '2019-09-11 11:42:42', '2019-09-11 11:42:42'),
(26, 12, '2', '2019-09-11 11:42:42', '2019-09-11 11:42:42'),
(27, 12, '3', '2019-09-11 11:42:42', '2019-09-11 11:42:42');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role` enum('admin','management','employee') COLLATE utf8mb4_unicode_ci DEFAULT 'employee',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `about` text COLLATE utf8mb4_unicode_ci,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `salary` double(8,2) NOT NULL DEFAULT '0.00',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','deactivate') COLLATE utf8mb4_unicode_ci DEFAULT 'deactivate',
  `sort` int(11) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role`, `name`, `email`, `email_verified_at`, `about`, `phone`, `salary`, `image`, `password`, `status`, `sort`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Admin', 'admin@admin.com', NULL, NULL, NULL, 0.00, NULL, '$2y$10$NFAxL8crngY4YVVxadPUeuq9RQtccQQ15fT5VccgqJIh4/PkS087u', 'active', 1, 'gZdC61mODlehYE5hnpkVjBF6dRHGGvLx5sRIF8QbelT6YSACY8EIb340o8v4', '2019-08-06 06:12:47', '2019-08-06 06:12:47'),
(3, 'management', 'Rony M', 'rony.mmj2@gmail.com', NULL, NULL, NULL, 0.00, NULL, '$2y$10$NFAxL8crngY4YVVxadPUeuq9RQtccQQ15fT5VccgqJIh4/PkS087u', 'active', 2, '1gw1YXwgSdLhk1O61bQdzjZgfypOouLvBKpIwXGJfmQIs1vJ8YScl6cMjKL4', '2019-08-06 06:12:47', '2019-08-06 06:12:47'),
(4, 'employee', 'Rony E', 'rony.mmj@gmail.com', NULL, NULL, NULL, 30000.00, NULL, '$2y$10$NFAxL8crngY4YVVxadPUeuq9RQtccQQ15fT5VccgqJIh4/PkS087u', 'active', 4, 'SNfR6OHfbpmIdg714Z4R3geIKmcQk6aEJtP37Ra2zl2KWERfJZYZj8BcW1qI', '2019-08-06 06:12:47', '2019-09-12 06:07:35'),
(5, 'employee', 'Hasan Ali', 'hasan@mediusware.com', NULL, NULL, NULL, 0.00, '1565152108.JPEG', '$2y$10$RTYSYoQPveoHoA/5S80QFuHHiNwFzeyJXk7pHgKZSNspEEQT7qJlS', 'active', 3, 'bsRfsiMpqoUd2JaDobX9IB48CIlaHdrcs5hpO9V8kfta4Zy8Zy7q8f1Ghil5', '2019-08-06 22:28:28', '2019-08-06 22:28:28'),
(6, 'management', 'Rashiqul Rony', 'rony.mmj2+2@gmail.com', NULL, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A ab aliquid aperiam dicta ex exercitationem expedita laudantium minima molestiae nam nemo, nostrum officia optio, quia, repudiandae tempora veniam voluptas voluptatibus.', '01738030343', 0.00, '1566361755.jpg', '$2y$10$fR5C3FSaHLCz8wOWClNRbOfY7LJB7ZpmiY66i1BUYjWDqG26UaUf6', 'active', 5, 'PbgUaapA2m7Z45kz2xzp6NJuotcuZbCOK3hcSVuQHYB8GXnnnMnjzz2PKDR2', '2019-08-20 22:29:15', '2019-08-21 02:58:26');

-- --------------------------------------------------------

--
-- Table structure for table `works`
--

CREATE TABLE `works` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `task_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `done_task_details` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `status` enum('pending','working','complete','rejected') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `sort` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `works`
--

INSERT INTO `works` (`id`, `task_id`, `employee_id`, `done_task_details`, `note`, `status`, `sort`, `created_at`, `updated_at`) VALUES
(1, 9, 4, '<p>Complete</p>', 'asdf asdf asdf asdf adsf adsfadf', 'complete', 0, '2019-08-07 05:23:51', '2019-08-19 04:34:51'),
(2, 2, 5, '<p><font color=\"#660000\"><span style=\"background-color: rgb(247, 250, 255);\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aperiam, aut consectetur distinctio earum enim eos ipsa ipsam iusto molestias omnis perferendis recusandae sint suscipit vero. Alias consectetur illo reprehenderit.``</span></font></p><pre style=\"color: rgb(0, 0, 0); font-family: &quot;Courier New&quot;; font-size: 10.5pt;\"><span style=\"font-style:italic;\">`</span></pre><pre style=\"color: rgb(0, 0, 0); font-family: &quot;Courier New&quot;; font-size: 10.5pt;\"><pre style=\"font-family: &quot;Courier New&quot;; font-size: 10.5pt; color: rgb(0, 0, 0);\"><span style=\"font-style: italic;\">$</span>(<span style=\"color: rgb(0, 0, 128); font-weight: bold;\">function </span>() {<br>    <span style=\"font-style: italic;\">$</span>( <span style=\"color: rgb(0, 128, 0); font-weight: bold;\">\"#tablecontents\" </span>).<span style=\"color: rgb(102, 14, 122); font-weight: bold;\">sortable</span>({<br>        <span style=\"color: rgb(102, 14, 122); font-weight: bold;\">items</span>: <span style=\"color: rgb(0, 128, 0); font-weight: bold;\">\"tr\"</span>,<br>        <span style=\"color: rgb(102, 14, 122); font-weight: bold;\">cursor</span>: <span style=\"color: rgb(0, 128, 0); font-weight: bold;\">\'move\'</span>,<br>        <span style=\"color: rgb(102, 14, 122); font-weight: bold;\">opacity</span>: <span style=\"color: rgb(0, 0, 255);\">0.6</span>,<br>        <span style=\"color: rgb(122, 122, 67);\">update</span>: <span style=\"color: rgb(0, 0, 128); font-weight: bold;\">function</span>() {<br>            <span style=\"font-style: italic;\">sendOrderToServer</span>();<br>        }<br>    });<br><br>    <span style=\"color: rgb(0, 0, 128); font-weight: bold;\">function </span><span style=\"font-style: italic;\">sendOrderToServer</span>() {<br>        <span style=\"color: rgb(0, 0, 128); font-weight: bold;\">var </span><span style=\"color: rgb(69, 131, 131);\">order </span>= [];<br><br>        <span style=\"font-style: italic;\">$</span>(<span style=\"color: rgb(0, 128, 0); font-weight: bold;\">\'</span><span style=\"color: rgb(0, 0, 128); font-weight: bold;\">tr</span><span style=\"color: rgb(0, 128, 0); font-weight: bold;\">.</span><span style=\"color: rgb(0, 0, 128); font-weight: bold;\">row1</span><span style=\"color: rgb(0, 128, 0); font-weight: bold;\">\'</span>).<span style=\"color: rgb(122, 122, 67);\">each</span>(<span style=\"color: rgb(0, 0, 128); font-weight: bold;\">function</span>(index,element) {<br>            <span style=\"color: rgb(69, 131, 131);\">order</span>.<span style=\"color: rgb(122, 122, 67);\">push</span>({<br>                <span style=\"color: rgb(102, 14, 122); font-weight: bold;\">id</span>: <span style=\"font-style: italic;\">$</span>(<span style=\"color: rgb(0, 0, 128); font-weight: bold;\">this</span>).<span style=\"color: rgb(122, 122, 67);\">attr</span>(<span style=\"color: rgb(0, 128, 0); font-weight: bold;\">\'data-id\'</span>),<br>                <span style=\"color: rgb(102, 14, 122); font-weight: bold;\">position</span>: index+<span style=\"color: rgb(0, 0, 255);\">1</span>,<br>            });<br>        });<br><br>        <span style=\"font-style: italic;\">$</span>.<span style=\"color: rgb(122, 122, 67);\">ajax</span>({<br>            <span style=\"color: rgb(102, 14, 122); font-weight: bold;\">type</span>: <span style=\"color: rgb(0, 128, 0); font-weight: bold;\">\"POST\"</span>,<br>            <span style=\"color: rgb(102, 14, 122); font-weight: bold;\">dataType</span>: <span style=\"color: rgb(0, 128, 0); font-weight: bold;\">\"json\"</span>,<br>            <span style=\"color: rgb(102, 14, 122); font-weight: bold;\">url</span>: <span style=\"color: rgb(0, 128, 0); font-weight: bold;\">\"</span><span style=\"background-color: rgb(247, 250, 255);\">{{ url(</span><span style=\"color: rgb(0, 128, 0); background-color: rgb(247, 250, 255); font-weight: bold;\">\'taskman/task/sortable\'</span><span style=\"background-color: rgb(247, 250, 255);\">) }}</span><span style=\"color: rgb(0, 128, 0); font-weight: bold;\">\"</span>,<br>            <span style=\"color: rgb(102, 14, 122); font-weight: bold;\">data</span>: {<br>                <span style=\"color: rgb(102, 14, 122); font-weight: bold;\">order</span>:<span style=\"color: rgb(69, 131, 131);\">order</span>,<br>                <span style=\"color: rgb(102, 14, 122); font-weight: bold;\">_token</span>: <span style=\"color: rgb(0, 128, 0); font-weight: bold;\">\'</span><span style=\"background-color: rgb(247, 250, 255);\">{{csrf_token()}}</span><span style=\"color: rgb(0, 128, 0); font-weight: bold;\">\'<br></span><span style=\"color: rgb(0, 128, 0); font-weight: bold;\">            </span>},<br>            <span style=\"color: rgb(122, 122, 67);\">success</span>: <span style=\"color: rgb(0, 0, 128); font-weight: bold;\">function</span>(response) {<br>                <span style=\"color: rgb(0, 0, 128); font-weight: bold;\">if </span>(response.<span style=\"color: rgb(102, 14, 122); font-weight: bold;\">status </span>== <span style=\"color: rgb(0, 128, 0); font-weight: bold;\">\"success\"</span>) {<br>                    <span style=\"color: rgb(102, 14, 122); font-weight: bold; font-style: italic;\">console</span>.<span style=\"color: rgb(122, 122, 67);\">log</span>(response);<br>                } <span style=\"color: rgb(0, 0, 128); font-weight: bold;\">else </span>{<br>                    <span style=\"color: rgb(102, 14, 122); font-weight: bold; font-style: italic;\">console</span>.<span style=\"color: rgb(122, 122, 67);\">log</span>(response);<br>                }<br>            }<br>        });<br>    }<br>});</pre></pre><pre style=\"color: rgb(0, 0, 0); font-family: &quot;Courier New&quot;; font-size: 10.5pt;\"><span style=\"font-style:italic;\">`</span></pre><pre style=\"color: rgb(0, 0, 0); font-family: &quot;Courier New&quot;; font-size: 10.5pt;\"><br></pre>', NULL, 'working', 0, '2019-08-07 22:51:04', '2019-08-07 22:51:04'),
(3, 11, 4, '<h2 id=\"purposes_data\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 21px 0px 0px; border: 0px; font-weight: bold; font-size: 17px; line-height: 21px; font-family: \"Helvetica Neue\", Helvetica, Arial, FreeSans, sans-serif; color: rgb(63, 63, 63);\">Dati personali raccolti per le seguenti finalità ed utilizzando i seguenti servizi:</h2><ul class=\"for_boxes cf\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; font-size: 16px; line-height: 1; font-family: \"Helvetica Neue\", Helvetica, Arial, FreeSans, sans-serif; list-style: none; zoom: 1; color: rgb(107, 107, 107);\"><li class=\"one_line_col\" style=\"margin: 0px; padding: 0px 0px 21px; border-width: 0px 0px 1px; border-top-style: initial; border-right-style: initial; border-bottom-style: dotted; border-left-style: initial; border-top-color: initial; border-right-color: initial; border-bottom-color: rgb(223, 223, 223); border-left-color: initial; border-image: initial; font-size: 13px; line-height: 19px; font-family: inherit; list-style: none; zoom: 1; float: left; width: 740px;\"><ul class=\"for_boxes\" style=\"margin-right: 0px; margin-left: 0px; padding: 0px; border: 0px; line-height: 1; font-family: inherit; list-style: none; zoom: 1;\"><li style=\"margin: 0px; padding: 0px; border: 0px; line-height: 19px; font-family: inherit; list-style: none; float: left; width: 370px; clear: left;\"><div class=\"iconed policyicon_purpose_16\" style=\"background-image: url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAMAAADXqc3KAAAAM1BMVEUAAAA%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz%2F10LmwAAAAEHRSTlMAECAwQFBgcICQoLDA0ODwVOCoyAAAAJFJREFUeF6V0NsOAyEIRVE6I4rFwvn%2Fr63N3CR10nQnPK2IUdpbpKmsorJQqOKTl2xeRhDsycMgA7QDGkmfq9cI%2FvNEhGcAO8CowAbAGTEwX1XDKvYNnJM7f78clVqfydOlgwRIG6S1TwDdQEnD3cv1iWw4f54VQ1qfUO5QDDGYVLNCmOQ5O2Ea8R2kP8FWobvefhoT%2FSVCMbAAAAAASUVORK5CYII%3D\"); padding-left: 40px; background-repeat: no-repeat; background-color: transparent; background-position: 2px 26px; margin-right: 15px;\"><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 24px 0px 0px; border: 0px; font-weight: bold; font-size: 13px; line-height: 19px; font-family: inherit; color: rgb(63, 63, 63);\">Accesso agli account su servizi terzi</h3><ul class=\"unstyled\" style=\"margin-right: 0px; margin-left: 0px; padding: 0px; border: 0px; line-height: 1; font-family: inherit; list-style: none;\"><li style=\"margin: 0px; padding: 0px; border: 0px; line-height: 19px; font-family: inherit; list-style: none;\"><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 10px 0px 0px; border: 0px; font-weight: bold; font-size: 13px; line-height: 19px; font-family: inherit; color: rgb(120, 120, 120);\">Accesso all\'account Facebook</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; line-height: 19px; font-family: inherit;\">Permessi: \'Su di me\' degli amici; Accesso ai dati privati; Email</p></li><li style=\"margin: 0px; padding: 0px; border: 0px; line-height: 19px; font-family: inherit; list-style: none;\"><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 10px 0px 0px; border: 0px; font-weight: bold; font-size: 13px; line-height: 19px; font-family: inherit; color: rgb(120, 120, 120);\">Accesso all\'account Dropbox e Accesso all\'account Google Drive</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; line-height: 19px; font-family: inherit;\">Dati Personali: varie tipologie di Dati secondo quanto specificato dalla privacy policy del servizio</p></li></ul></div></li><li style=\"margin: 0px; padding: 0px; border: 0px; line-height: 19px; font-family: inherit; list-style: none; float: left; width: 370px; clear: right;\"><div class=\"iconed policyicon_purpose_10\" style=\"background-image: url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAMAAADXqc3KAAAAM1BMVEUAAAA%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz%2F10LmwAAAAEHRSTlMAECAwQFBgcICQoLDA0ODwVOCoyAAAAKVJREFUeF51jlmWwyAMBGXEboT6%2FqedIZAAJqnfer3QJKpGOrkKakW5noIrAlFA5V0EKL%2B8Iqw1d%2B%2FojflTx4JlNUJGnVe1tOBUfRMZYmjDCDKRINFBglCLnXiltnTClfAtEgACxvHJldHF4xYL3gLq1l1Mgfk5AZtQx%2FYfdroL4TySXFeRWTAQc0%2Fhe0FHbRiicsJGZG3iNgUPiimgYBUHlQP94g9%2BZg8xOTGEFAAAAABJRU5ErkJggg%3D%3D\"); padding-left: 40px; background-repeat: no-repeat; background-color: transparent; background-position: 2px 26px; margin-left: 15px;\"><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 24px 0px 0px; border: 0px; font-weight: bold; font-size: 13px; line-height: 19px; font-family: inherit; color: rgb(63, 63, 63);\">Contattare l\'Utente</h3><ul class=\"unstyled\" style=\"margin-right: 0px; margin-left: 0px; padding: 0px; border: 0px; line-height: 1; font-family: inherit; list-style: none;\"><li style=\"margin: 0px; padding: 0px; border: 0px; line-height: 19px; font-family: inherit; list-style: none;\"><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 10px 0px 0px; border: 0px; font-weight: bold; font-size: 13px; line-height: 19px; font-family: inherit; color: rgb(120, 120, 120);\">Mailing list o newsletter</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; line-height: 19px; font-family: inherit;\">Dati Personali: cognome; email; nome</p></li></ul></div></li><li style=\"margin: 0px; padding: 0px; border: 0px; line-height: 19px; font-family: inherit; list-style: none; float: left; width: 370px; clear: left;\"><div class=\"iconed policyicon_purpose_18\" style=\"background-image: url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYBAMAAAASWSDLAAAAGFBMVEUAAAA%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz9%2BjSc3AAAAB3RSTlMAEEBQgMzQxeXuPgAAADJJREFUGFdjYMAJWEPhIAAPh70cDgoGK6cI5B8Yp6S8TACJk4gkA5RAcBKR9BQLoAUOAATNYYOCulUNAAAAAElFTkSuQmCC\"); padding-left: 40px; background-repeat: no-repeat; background-color: transparent; background-position: 2px 26px; margin-right: 15px;\"><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 24px 0px 0px; border: 0px; font-weight: bold; font-size: 13px; line-height: 19px; font-family: inherit; color: rgb(63, 63, 63);\">Gestione contatti e invio di messaggi</h3><ul class=\"unstyled\" style=\"margin-right: 0px; margin-left: 0px; padding: 0px; border: 0px; line-height: 1; font-family: inherit; list-style: none;\"><li style=\"margin: 0px; padding: 0px; border: 0px; line-height: 19px; font-family: inherit; list-style: none;\"><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 10px 0px 0px; border: 0px; font-weight: bold; font-size: 13px; line-height: 19px; font-family: inherit; color: rgb(120, 120, 120);\">Mailchimp</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; line-height: 19px; font-family: inherit;\">Dati Personali: cognome; Dati di utilizzo; email; nome; varie tipologie di Dati</p></li></ul></div></li><li style=\"margin: 0px; padding: 0px; border: 0px; line-height: 19px; font-family: inherit; list-style: none; float: left; width: 370px; clear: right;\"><div class=\"iconed policyicon_purpose_23\" style=\"background-image: url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYBAMAAAASWSDLAAAAGFBMVEUAAAA%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz9%2BjSc3AAAAB3RSTlMAEEBQgMzQxeXuPgAAADJJREFUGFdjYMAJWEPhIAAPh70cDgoGK6cI5B8Yp6S8TACJk4gkA5RAcBKR9BQLoAUOAATNYYOCulUNAAAAAElFTkSuQmCC\"); padding-left: 40px; background-repeat: no-repeat; background-color: transparent; background-position: 2px 26px; margin-left: 15px;\"><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 24px 0px 0px; border: 0px; font-weight: bold; font-size: 13px; line-height: 19px; font-family: inherit; color: rgb(63, 63, 63);\">Heat mapping e registrazione sessioni</h3><ul class=\"unstyled\" style=\"margin-right: 0px; margin-left: 0px; padding: 0px; border: 0px; line-height: 1; font-family: inherit; list-style: none;\"><li style=\"margin: 0px; padding: 0px; border: 0px; line-height: 19px; font-family: inherit; list-style: none;\"><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 10px 0px 0px; border: 0px; font-weight: bold; font-size: 13px; line-height: 19px; font-family: inherit; color: rgb(120, 120, 120);\">Hotjar Heat Maps & Recordings</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; line-height: 19px; font-family: inherit;\">Dati Personali: Cookie; Dati di utilizzo; varie tipologie di Dati secondo quanto specificato dalla privacy policy del servizio</p></li><li style=\"margin: 0px; padding: 0px; border: 0px; line-height: 19px; font-family: inherit; list-style: none;\"><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 10px 0px 0px; border: 0px; font-weight: bold; font-size: 13px; line-height: 19px; font-family: inherit; color: rgb(120, 120, 120);\">Crazy Egg</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; line-height: 19px; font-family: inherit;\">Dati Personali: Cookie; Dati di utilizzo</p></li></ul></div></li><li style=\"margin: 0px; padding: 0px; border: 0px; line-height: 19px; font-family: inherit; list-style: none; float: left; width: 370px; clear: left;\"><div class=\"iconed policyicon_purpose_37\" style=\"background-image: url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYBAMAAAASWSDLAAAAGFBMVEUAAAA%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz9%2BjSc3AAAAB3RSTlMAEEBQgMzQxeXuPgAAADJJREFUGFdjYMAJWEPhIAAPh70cDgoGK6cI5B8Yp6S8TACJk4gkA5RAcBKR9BQLoAUOAATNYYOCulUNAAAAAElFTkSuQmCC\"); padding-left: 40px; background-repeat: no-repeat; background-color: transparent; background-position: 2px 26px; margin-right: 15px;\"><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 24px 0px 0px; border: 0px; font-weight: bold; font-size: 13px; line-height: 19px; font-family: inherit; color: rgb(63, 63, 63);\">Interazione con le piattaforme di live chat</h3><ul class=\"unstyled\" style=\"margin-right: 0px; margin-left: 0px; padding: 0px; border: 0px; line-height: 1; font-family: inherit; list-style: none;\"><li style=\"margin: 0px; padding: 0px; border: 0px; line-height: 19px; font-family: inherit; list-style: none;\"><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 10px 0px 0px; border: 0px; font-weight: bold; font-size: 13px; line-height: 19px; font-family: inherit; color: rgb(120, 120, 120);\">Widget di Drift</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; line-height: 19px; font-family: inherit;\">Dati Personali: Cookie; Dati comunicati durante l\'utilizzo del servizio; Dati di utilizzo; varie tipologie di Dati secondo quanto specificato dalla privacy policy del servizio</p></li></ul></div></li><li style=\"margin: 0px; padding: 0px; border: 0px; line-height: 19px; font-family: inherit; list-style: none; float: left; width: 370px; clear: right;\"><div class=\"iconed policyicon_purpose_26\" style=\"background-image: url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYBAMAAAASWSDLAAAAGFBMVEUAAAA%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz9%2BjSc3AAAAB3RSTlMAEEBQgMzQxeXuPgAAADJJREFUGFdjYMAJWEPhIAAPh70cDgoGK6cI5B8Yp6S8TACJk4gkA5RAcBKR9BQLoAUOAATNYYOCulUNAAAAAElFTkSuQmCC\"); padding-left: 40px; background-repeat: no-repeat; background-color: transparent; background-position: 2px 26px; margin-left: 15px;\"><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 24px 0px 0px; border: 0px; font-weight: bold; font-size: 13px; line-height: 19px; font-family: inherit; color: rgb(63, 63, 63);\">Interazione con social network e piattaforme esterne</h3><ul class=\"unstyled\" style=\"margin-right: 0px; margin-left: 0px; padding: 0px; border: 0px; line-height: 1; font-family: inherit; list-style: none;\"><li style=\"margin: 0px; padding: 0px; border: 0px; line-height: 19px; font-family: inherit; list-style: none;\"><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 10px 0px 0px; border: 0px; font-weight: bold; font-size: 13px; line-height: 19px; font-family: inherit; color: rgb(120, 120, 120);\">Pulsante +1 e widget sociali di Google+</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; line-height: 19px; font-family: inherit;\">Dati Personali: Cookie; Dati di utilizzo</p></li></ul></div></li><li style=\"margin: 0px; padding: 0px; border: 0px; line-height: 19px; font-family: inherit; list-style: none; float: left; width: 370px; clear: left;\"><div class=\"iconed policyicon_purpose_9\" style=\"background-image: url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAQAAABKfvVzAAACC0lEQVQ4y7XSO2gWVhQH8BPxDRG%2BEhurMcSI4GsoPqjkZ6BLwcFFHUQJKqbEwRciDqZDF90cpIsILtZHh0KTIdQMgkTRiIshBoWgTRpbsVaxgqRf4uM4JCHfRzpIwXun8%2Bf%2BuHDOifj%2FxwoD2qek7Qat%2FG9Qr1%2FblLRNv%2FqyqKHCjIgIqw3oGE9mmtlQERGhw4DVERFmNFREhG91uq6gxUspnVdlky5dNqlyXkovtSi4rtPe8JeUaq1yWLN9tkVoklJThK1a7HXISrVSehpSGrXb5woWqFZljZNSOmmtBRapUe0Lu4xKOQZSr0633dejS7chKQ25p0%2BvHn3u6Bt7OQFSeuWG3pI6DbvpZ5dc8WwimwTPbYswx49Sei89sDNCpaoI6%2FyqWA5OmxUR4StF6Z0hX5puvyH%2FOmeeudrLwXfjg1prUCo6FuGyty444W89CpYZKQU%2FmF3ywwvVthtxwpwImz1yzjSdpWBYq2nWuzbWoQgX%2FaPOAd%2Br1O55hDOl4LHdDRXqnPVWehLhlPSNgiURFlof4adJMGC7eRERarRKr32t2qBn9lhlg%2BVq7fDbJDhasp%2BfueW9brOscdULv7vntlselnZpadlKH5fSRYvN16ytdJgT4KBGGzVqtNFmv4yndzWrt8WjqSCNGFZUNOxN2Xq8K6%2FD47Et%2FKg7ajAc9edHgz8ciU9%2BPgBKt4%2FTzlslzAAAAABJRU5ErkJggg%3D%3D\"); padding-left: 40px; background-repeat: no-repeat; background-color: transparent; background-position: 2px 26px; margin-right: 15px;\"><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 24px 0px 0px; border: 0px; font-weight: bold; font-size: 13px; line-height: 19px; font-family: inherit; color: rgb(63, 63, 63);\">Registrazione ed autenticazione</h3><ul class=\"unstyled\" style=\"margin-right: 0px; margin-left: 0px; padding: 0px; border: 0px; line-height: 1; font-family: inherit; list-style: none;\"><li style=\"margin: 0px; padding: 0px; border: 0px; line-height: 19px; font-family: inherit; list-style: none;\"><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 10px 0px 0px; border: 0px; font-weight: bold; font-size: 13px; line-height: 19px; font-family: inherit; color: rgb(120, 120, 120);\">Instagram Authentication, OneDrive OAuth e Google OAuth</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; line-height: 19px; font-family: inherit;\">Dati Personali: varie tipologie di Dati secondo quanto specificato dalla privacy policy del servizio</p></li></ul></div></li><li style=\"margin: 0px; padding: 0px; border: 0px; line-height: 19px; font-family: inherit; list-style: none; float: left; width: 370px; clear: right;\"><div class=\"iconed policyicon_purpose_32\" style=\"background-image: url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYBAMAAAASWSDLAAAAGFBMVEUAAAA%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz8%2FPz9%2BjSc3AAAAB3RSTlMAEEBQgMzQxeXuPgAAADJJREFUGFdjYMAJWEPhIAAPh70cDgoGK6cI5B8Yp6S8TACJk4gkA5RAcBKR9BQLoAUOAATNYYOCulUNAAAAAElFTkSuQmCC\"); padding-left: 40px; background-repeat: no-repeat; background-color: transparent; background-position: 2px 26px; margin-left: 15px;\"><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 24px 0px 0px; border: 0px; font-weight: bold; font-size: 13px; line-height: 19px; font-family: inherit; color: rgb(63, 63, 63);\">Remarketing e behavioral targeting</h3><ul class=\"unstyled\" style=\"margin-right: 0px; margin-left: 0px; padding: 0px; border: 0px; line-height: 1; font-family: inherit; list-style: none;\"><li style=\"margin: 0px; padding: 0px; border: 0px; line-height: 19px; font-family: inherit; list-style: none;\"><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 10px 0px 0px; border: 0px; font-weight: bold; font-size: 13px; line-height: 19px; font-family: inherit; color: rgb(120, 120, 120);\">Remarketing Google Ads</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; line-height: 19px; font-family: inherit;\">Dati Personali: Cookie; Dati di utilizzo</p></li></ul></div></li></ul></li></ul>', NULL, 'pending', 0, '2019-08-20 22:43:08', '2019-09-12 05:03:15'),
(4, 4, 5, '<p><span style=\"font-size: 0.875rem;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias aperiam debitis deserunt earum excepturi fugit hic libero magnam, nostrum officia perspiciatis quibusdam reiciendis sed sunt temporibus tenetur totam ut voluptatibus!</span><br></p>', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.', 'complete', 0, '2019-08-21 03:08:08', '2019-08-21 03:08:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fcm_tokens`
--
ALTER TABLE `fcm_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `late_reports`
--
ALTER TABLE `late_reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leaves`
--
ALTER TABLE `leaves`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leave_applications`
--
ALTER TABLE `leave_applications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leave_templates`
--
ALTER TABLE `leave_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `task_employee_relations`
--
ALTER TABLE `task_employee_relations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `task_lists`
--
ALTER TABLE `task_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `works`
--
ALTER TABLE `works`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fcm_tokens`
--
ALTER TABLE `fcm_tokens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `late_reports`
--
ALTER TABLE `late_reports`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `leaves`
--
ALTER TABLE `leaves`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `leave_applications`
--
ALTER TABLE `leave_applications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `leave_templates`
--
ALTER TABLE `leave_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `task_employee_relations`
--
ALTER TABLE `task_employee_relations`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `task_lists`
--
ALTER TABLE `task_lists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `works`
--
ALTER TABLE `works`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
