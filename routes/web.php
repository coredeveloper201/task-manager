<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WebsiteController@index');

Route::post('/login', 'Auth\LoginController@login');

Auth::routes();

//Admin Route
Route::group(['prefix' => 'taskman', 'middleware' => 'auth'], function(){
    Route::post('users/registerFcmToken', 'Admin\DashboardController@registerFcmToken');

    Route::get('dashboard', 'Admin\DashboardController@index')->name('home');
    Route::get('media/image/{id}/delete', 'Admin\DashboardController@taskImageDelete');


    Route::get('work-list', 'Admin\WorkController@workList');
    Route::get('work/create/{task_id}', 'Admin\WorkController@create');
    Route::resource('work', 'Admin\WorkController');
    Route::post('work/sortable', 'Admin\WorkController@sortable');
    // review
    Route::resource('review', 'Admin\ReviewController');
    Route::post('review/update/{id}', 'Admin\ReviewController@update');

    //Late report
    Route::get('emp-late-report/list', 'Admin\LateReportController@empReportList')->name('emp.list');
    Route::get('emp-monthly-late-report', 'Admin\LateReportController@empMonthlyReportList')->name('emp-late-report.monthly');
    //Leave Application
    Route::resource('leave-application', 'Admin\LeaveApplicationController');

    Route::group(['middleware' => 'management' || 'admin'], function(){
        Route::resource('user', 'Admin\UserController');
        Route::post('update-password/{id}', 'Admin\UserController@updatePassword')->name('user.update-password');
        Route::post('user/sortable', 'Admin\UserController@sortable');
        Route::post('user/reset-password/{id}', 'Admin\UserController@resetPassword');
        Route::post('user/reset-email/{id}', 'Admin\UserController@resetEmail');

        Route::resource('project', 'Admin\ProjectController');
        Route::post('user/project', 'Admin\ProjectController@sortable');

        Route::resource('task', 'Admin\TaskController');
        Route::post('task/sortable', 'Admin\TaskController@sortable');

        //Late report
        Route::get('late-report/list', 'Admin\LateReportController@reportList')->name('late_report.list');
        Route::get('monthly-late-report', 'Admin\LateReportController@monthlyReportList')->name('late-report.monthly');
        Route::get('late-report/filter', 'Admin\LateReportController@filter');
        Route::resource('late-report', 'Admin\LateReportController');

        //Leave
        Route::post('/leave/update', 'Admin\LeaveController@leaveUpdate');
        Route::get('/yearly-leave-report', 'Admin\LeaveController@yearlyLeaveReport');
        Route::resource('leave', 'Admin\LeaveController');

        //Application
        Route::get('casualToSick/{id}', 'Admin\LeaveApplicationController@casualToSick');
        Route::get('sickToCasual/{id}', 'Admin\LeaveApplicationController@sickToCasual');

        Route::get('emp-application', 'Admin\LeaveApplicationController@empApplication')->name('emp.application');
        Route::get('/application/approve/{id}', 'Admin\LeaveApplicationController@applicationApprove');
        Route::get('/application/reject/{id}', 'Admin\LeaveApplicationController@applicationReject');
        Route::resource('application', 'Admin\LeaveApplicationController');
        Route::resource('leave-template', 'Admin\LeaveTemplateController');


        Route::resource('emp-note', 'Admin\EmployeeNoteController');

        //Salary Report
        Route::get('/salary', 'Admin\SalaryController@index');
        Route::get('/year-end-salary', 'Admin\SalaryController@yearEnd');


        //Questionnaire
        Route::resource('/exam', 'Admin\ExamController');
        Route::get('/exam/questions/{exam_id}', 'Admin\ExamController@showQuestions')->name('exam.questions');
        Route::get('/exam/examine/{exam_id}', 'Admin\ExamController@showExamine')->name('exam.show-examine'); // Show Examine List along with Exam ID
        Route::post('/exam/change-activity/{id}', 'Admin\ExamController@changeActivity')->name('exam.change-activity');
        Route::resource('/question', 'Admin\ExamQuestionController');
        Route::post('/question/delete-image/{question}', 'Admin\ExamQuestionController@deleteImage');

        //Admin
        Route::resource('/applicant', 'Admin\ApplicantController');
        Route::get('/applicant/remark_show/{id}', 'Admin\ApplicantController@remark');

        // Exam Answers
        Route::get('/exam/answer/{applicant_id}', 'Admin\ExamAnswerController@showAnswers')->name('exam-answer.show');

    });

});

// This is a test route
Route::get('/interview-email', function (){
    $applicant = \App\Models\Applicant::first();
    return view('admin.exam.email', compact('applicant'));
});
// End of test route

Route::get('/interview-exam/{exam_slug}', 'Admin\ApplicantAnswerController@show')->name('interview-exam.show');
Route::post('/interview-exam/submit/{exam_id}', 'Admin\ApplicantAnswerController@submitAnswer')->name('interview-exam.submit');
